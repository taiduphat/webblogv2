Webblog Admin site has features:
+ manage templates
+ magage images
+ manage descendants items

Solution webblogApi:
- Backend project:
    + ASP.NET Core
    + using gRPC to implement webblog service
- Integration test project:
    + Xunit to build integration test

- Authentication project:
    + using IdentityServer4 to build authentication server
    
- Front end project (just built admin pages)
    + using angular 8
    + using gRPC-Web library to access a gRPC service
    

How to deploy these apps on docker:
+ Sets IP_ADDRESS to your Ipv4 address on '.env' file
+ Opens cmd in current docker-compose file location, runs command 'docker-compose up' to deploy to docker.