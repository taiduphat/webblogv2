export const environment = {
  production: true,
  apiUrl: window["env"]["apiUrl"],
  authorityUrl: window["env"]["authorityUrl"],
};
