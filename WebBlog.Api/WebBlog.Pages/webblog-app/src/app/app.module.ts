import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { AuthGuard } from './admin/components/auth/auth.guard';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthCallbackComponent } from './admin/components/auth/auth-callback.component';
import { authModuleConfig } from './admin/components/auth/auth-module.config';

// Define routes
const routes: Routes = [
  { path: '', loadChildren: './admin/admin.module#AdminModule', canActivate: [AuthGuard] },
  { path: 'auth-callback', component: AuthCallbackComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    AuthCallbackComponent
  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    OAuthModule.forRoot(authModuleConfig),
    // Register routing
    [RouterModule.forRoot(routes)]
  ],
  providers: [AuthGuard, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
