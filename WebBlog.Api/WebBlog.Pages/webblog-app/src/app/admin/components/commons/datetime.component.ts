import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { hasRequiredField } from './common';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-datetime',
    styles: [`

    `],
    template: `        
        <div class="input-group">
            <input ngbDatepicker readonly #d="ngbDatepicker" [ngModel]="ngbDate" (dateSelect)="onDateSelect($event)"
                [class.is-invalid]="required && !pFormControl.valid && pFormControl.touched"
                class="form-control col-6">
            
            <div class="input-group-append">
                <a class="btn btn-outline-secondary" (click)="d.toggle()"> 
                    <i class="fa fa-calendar"></i>
                </a>
            </div>
        </div>
        <div *ngIf="required" class="invalid-feedback">{{labelText}} is required</div>
    `
})
export class DatetimeComponent implements OnInit {
    
    @Input() labelText: string;
    @Input() pFormControl: FormControl;
    
    private ngbDate: NgbDateStruct;
    private required: boolean;

    ngOnInit(): void {
        this.required = hasRequiredField(this.pFormControl);
        
        let dateStr = this.pFormControl.value;
        if (dateStr != null){
            let date = new Date(dateStr);
            this.ngbDate = {
                day: date.getDay(),
                month: date.getMonth(),
                year: date.getFullYear()
            }
        }
    }

    onDateSelect(ngbDate: NgbDateStruct){
        let date = new Date(ngbDate.year, ngbDate.month, ngbDate.day);
        this.pFormControl.setValue(date.toISOString());
    }
}