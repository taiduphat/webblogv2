import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({  
    name : 'callFunc'  
})  
export class CallFuncPipe implements PipeTransform {  
    transform(func: Function, ...args: any[]) {  
        return func.apply(this, args);
    }  
}  