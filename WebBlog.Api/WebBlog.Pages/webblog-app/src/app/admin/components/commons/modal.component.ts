import { Component, ElementRef, ViewChild, OnInit, Directive, HostListener, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { trigger } from '@angular/animations';
import { hideAnimation } from '../animations/animations';

@Component({
    selector: 'app-modal',
    styles: [`
        .modal {
            background-color:rgba(0, 0, 0, 0.5);
        }
        .modal-header, .modal-footer {
            padding: 10px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                margin-top: 15%;
            }
        }
    `],
    animations: [
        trigger('toggle', hideAnimation(1)),
    ],
    template:`
        <div class="modal fade" #appModal [@toggle]="isHide">
            <div class="modal-dialog {{classList}}">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-info text-white">
                        <h4 class="modal-title">{{modalTitle}}</h4>
                        <button type="button" class="close" (click)="closeModal()" style="opacity:1">&times;</button>
                    </div>
                    <div class="modal-body">
                       <ng-content></ng-content>
                    </div>
                    <div class="modal-footer">
                        <a (click)="onYesClick()" class="btn btn-primary btn-sm">Yes</a>
                        <a (click)="closeModal()" class="btn btn-light btn-sm">No</a>
                    </div>
                </div>
            </div>
        </div>
    `
  })

export class ModalComponent implements OnDestroy {
   
    @Input() modalTitle: string;
    @Input() classList: any;

    private callBackDef: {
        callBackFn: (...args: any[]) => void,
        args: any
    }

    @ViewChild('appModal', { static: true }) private elRef: ElementRef;
    private isHide: boolean;

    constructor(private el: ElementRef){
        document.body.appendChild(this.el.nativeElement);
        this.isHide = true;
    }
    
    ngOnDestroy(): void {
        this.el.nativeElement.remove();
    }
    
    showModal(callback: (...args: any[]) => void, ...args: any[]): void {
        this.elRef.nativeElement.classList.add("d-block");
        this.isHide = false;

        this.callBackDef = {
            callBackFn: callback,
            args: args
        };
    }

    closeModal(): void {
        this.elRef.nativeElement.classList.remove("d-block");
        this.isHide = true;
    }

    private onYesClick() {
        this.callBackDef.callBackFn.apply(this, this.callBackDef.args)
    }
}