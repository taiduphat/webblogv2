import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-upload',
    styles: [`
        .input-group-addon {
            padding: 0;
            border: none;
        }
    `],
    template: `
        <form [formGroup]="uploadForm" (submit)="onSubmit()">
            <div class="input-group">
                <input type="hidden" [formControl]="uploadForm.controls.file"/>
                <input type="file" accept="{{acceptExtension}}" 
                    (change)="onFileChange($event)" class="form-control">
                <div class="input-group-addon">
                    <input type="submit" [disabled]="uploadForm.invalid" class="btn btn-primary"/>
                </div>
            </div>
            <div>
                <input [formControl]="uploadForm.controls.isOverwrite" type="checkbox"><span style="font-size: medium"> Overwrite image if exists</span><br>
            </div>
        </form>
    `
})
export class UploadImageComponent {
    @Output("onUpload") submited = new EventEmitter<FileInfo>();
    
    acceptExtension = ".gif,.jpg,.jpeg,.png";
    uploadForm: FormGroup;

    constructor(private formBuilder: FormBuilder){
        this.uploadForm = this.formBuilder.group({
            file: [null, Validators.required],
            fileName: null,
            isOverwrite: false
        }) 
    }

    onSubmit(){
        let fileName = this.uploadForm.controls.fileName.value;
        let fileBuffer = this.uploadForm.controls.file.value;
        let isOverwrite = this.uploadForm.controls.isOverwrite.value;
        this.submited.emit(new FileInfo (fileName, fileBuffer, isOverwrite));
    }

    onFileChange(event) {
        const reader = new FileReader();
        if(event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            let fileName = file.name;
            
            reader.readAsArrayBuffer(file);
            reader.onload = () => {
                let arrayBuffer = reader.result as ArrayBuffer;
                this.uploadForm.patchValue({
                    file: new Uint8Array(arrayBuffer),
                    fileName: fileName
                })
            };
        }
      }
}

export class FileInfo {
    name: string;
    buffer: Uint8Array;
    isOverwrite: boolean;

    constructor(name, buffer, isOverwrite: boolean) {
        this.name = name;
        this.buffer = buffer;
        this.isOverwrite = isOverwrite || false;
    }
}