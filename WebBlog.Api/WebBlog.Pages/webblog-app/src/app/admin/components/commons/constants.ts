export const URL_CONSTANT = {
    Dashboard: {
        PATH: '',
    },
    Template: {
        PATH: 'template',
        ADD_URL: 'add',
        EDIT_URL: 'edit/:id',
        LIST_URL: 'list'
    },
    Image: {
        PATH: 'image',
        LIST_URL: 'list'
    },
    Item: {
        PATH: 'item',
        LIST_URL: 'list',
        ADD_URL: 'add',
        EDIT_URL: 'edit/:id',
    }

}

export const ENTITY_CONSTANT = {
    FieldType: {
        ITEM_REFERENCE: 'ccbeb21c-bd3a-4c75-92c0-d8f83467cb67',
        ITEM_SET_REFERENCE: '473f7834-f806-4049-bb20-41aecf29c388',
        BOOL: '1b18f811-ebab-49c4-98f7-20df944ba876',
        DATE_TIME: '8c630622-45a0-4eda-8291-c70b47d290fb',
        NUMBER: '94d86abc-5a4b-46ac-be43-ddeebd153290',
        PLAIN_TEXT: '6201a71d-b4b6-43f9-9076-9a3218732422',
        RICH_TEXT: '37ba6712-3043-4392-a447-0d9cf8fa59d8',
        IMAGE_REFERENCE: '451a2d88-1ff6-47d8-91aa-29aa210eac6a'
    },
    Template: {
        FOLDER_TEMPLATE: '5606052c-1fe6-4859-b894-dd3826b0ecc9'
    }
}

export enum FormElement {
    Unknown = 0,
    Bool = 1,
    MultipleSelect,
    Select,
    DateTime,
    Number,
    PlainText,
    RichText,
    Image
}
