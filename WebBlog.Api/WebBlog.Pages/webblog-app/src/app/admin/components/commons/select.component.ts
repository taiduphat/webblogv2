import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { hasRequiredField } from './common';

@Component({
    selector: 'app-select',
    styles: [`
        .error-msg {
            display: none;
        }
        .has-error .error-msg {
            display: block;
        }
    `],
    template: `
        <section>
            <select #elRef [class.is-invalid]="required && !pFormControl.valid && pFormControl.touched" class="form-control" [formControl]="pFormControl">
                <option *ngIf="defaultLabel" selected value="">--- Select ---</option>
                <option *ngFor="let item of selectList?.items" value="{{item?.value}}">{{item?.text}}</option>
            </select>
            
            <div class="invalid-feedback">Please select this field</div>
        </section>
    `
})
export class SelectComponent {
    
    @Input() pFormControl: FormControl;
    @Input() selectList: SelectList;
    @Input() defaultLabel: boolean = false;
    @Input() multiple: boolean = false;

    private required: boolean;
    
    @ViewChild('elRef', {static: true}) private elRef: ElementRef;

    ngOnInit(): void {
        this.required = hasRequiredField(this.pFormControl);
        if (this.multiple){
            this.elRef.nativeElement.setAttribute('multiple', null);
        }
    }
}


export class SelectList {
    items: Array<SelectListItem> = [];

    constructor(array?: Array<any> | any[], 
        textProperty?: string, 
        valueProperty?: string) {
            
            if (array == null || array.length == 0) 
                return;

            array.forEach(item => {
                this.items.push(new SelectListItem(item[textProperty], item[valueProperty]));
            })
    }
}
export class SelectListItem {
    text: string;
    value: string;

    constructor(text:string, value:string) {
        this.text = text;
        this.value = value;
    }
}