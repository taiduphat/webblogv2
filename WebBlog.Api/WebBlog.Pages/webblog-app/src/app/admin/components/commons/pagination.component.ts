import { 
    Component, Input, OnChanges, Output, EventEmitter, Directive, HostListener, SimpleChanges, OnDestroy 
} from '@angular/core';
import { PagedList } from '../../services/webblog.dto';


@Component({
    selector: 'app-pagination',
    styles: [`
        .page-link {
            cursor: pointer;
        }
    `],
    template: `
        <ul class="pagination pagination-sm pull-right" paging>
          <li *ngFor="let item of pagination" [class.active]="item?.isActive">
            <a class="page-link" [attr.data-page]="item?.page">{{item?.text}}</a>
          </li>
        </ul>
    `
})
@Directive({selector: '[paging] a'})
export class PaginationComponent implements OnChanges {
   
    @Input() pagedList: PagedList<any>;
    @Output() changePage = new EventEmitter<number>();

    pagination: Array<any>;

    ngOnChanges(changes: SimpleChanges): void {
        this.pagination = [];
        if (this.pagedList != null) {
            let current = this.pagedList.currentPage;
            let minPage = 1;
            let maxPage =  this.pagedList.pageCount;

            let start = Math.max(current - 2, minPage);
            let end = Math.min(current + 2, maxPage);
            
            if (start != minPage){
                this.pagination.push({
                    page: 1,
                    text: "<<"
                });
            }
            for(let i = start; i <= end; i++){
                this.pagination.push({
                    page: i,
                    text: i,
                    isActive: i == current
                });
            }
            if (end != maxPage){
                this.pagination.push({
                    page: maxPage,
                    text: ">>"
                });
            }
        }
    }

    @HostListener('click', ['$event']) onClick(event) {
        let selectedPage = event.target.getAttribute('data-page');
        this.changePage.emit(selectedPage);
    }
}