import { Component, Input, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { hasRequiredField } from './common';

@Component({
    selector: 'app-input',
    styles: [`
        ckeditor.is-invalid ~.invalid-feedback {
            display: block;
        }
    `],
    template: `
        <input *ngIf="!ckEditor" type="text" 
            [class.is-invalid]="required && !pFormControl.valid && pFormControl.touched"
            class="form-control" [formControl]="pFormControl" placeholder="{{labelText}}">
        
        <ckeditor *ngIf="ckEditor" [class.is-invalid]="required && !pFormControl.valid && pFormControl.touched"
            [editor]="Editor" [formControl]="pFormControl"></ckeditor>
        
        <div class="invalid-feedback">{{labelText}} is required</div>
    `
})
export class InputComponent implements OnInit {
    
    @Input() labelText: string;
    @Input() pFormControl: FormControl;
    @Input() ckEditor: boolean = false;

    private Editor = ClassicEditor;
    private required: boolean;

    ngOnInit(): void {
        this.required = hasRequiredField(this.pFormControl);
    }
}