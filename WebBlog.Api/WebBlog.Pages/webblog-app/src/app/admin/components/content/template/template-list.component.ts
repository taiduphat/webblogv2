import { Component, OnInit, ViewChild } from '@angular/core';
import { WebBlogService } from 'src/app/admin/services/webblog.service';
import { TemplateDto, StatusResultDto } from 'src/app/admin/services/webblog.dto';
import { ModalComponent } from '../../commons/modal.component';

@Component({
  templateUrl: './template-list.component.html',
})
export class TemplateListComponent implements OnInit {
  
  templateList: Array<TemplateDto>;
  statusResult: StatusResultDto;
  @ViewChild(ModalComponent, {static: false}) modal: ModalComponent;

  constructor(private webblogService: WebBlogService){
  }

  ngOnInit(): void {
    this.webblogService.getTemplates().then(result => {
      this.templateList = result;
    });
  }

  onRemoveClick(template: TemplateDto): void {
    this.modal.showModal(async (temp: TemplateDto) => {
      
      this.statusResult = await this.webblogService.removeTemplate(temp.id);
      if (this.statusResult.isSuccess) {
        let index = this.templateList.indexOf(temp);
        this.templateList = this.templateList.splice(index, 1);
      }

      this.modal.closeModal();
    }, template);
  }
}