import { Component, OnInit } from '@angular/core';
import { ImageDto, PagedList } from 'src/app/admin/services/webblog.dto';
import { WebBlogService } from 'src/app/admin/services/webblog.service';
import { FileInfo } from '../../commons/upload-image.component';


@Component({
    templateUrl: './image-manager.component.html',
  })
export class ImageManagerComponent implements OnInit {
    imageList: PagedList<ImageDto>;

    constructor(private webblogService: WebBlogService){
    }
  
    async ngOnInit() {
      this.imageList = await this.webblogService.getImages();
    }

    onUpload(fileInfo: FileInfo): void {
      let image: ImageDto = {
        fileName: fileInfo.name,
        content: fileInfo.buffer,
        isOverwrite: fileInfo.isOverwrite
      };
      
      this.webblogService.uploadImage(image).then(result => {
        alert('Upload image: ' + result.message);
      });
    }

    async onChangePage(page: number){
      this.imageList = await this.webblogService.getImages(page);
    }

    async onRemoveImage(index: number){
      let selectedImage = this.imageList.results[index];
      let response = await this.webblogService.removeImage(selectedImage.id);
      if (response.isSuccess){
        this.imageList.results.splice(index, 1);
      }
    }
}