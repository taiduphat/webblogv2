import { Component, OnInit, ViewChild, Renderer2, ElementRef, Input, Directive } from '@angular/core';
import { WebBlogService } from 'src/app/admin/services/webblog.service';
import { SelectList } from '../../commons/select.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalComponent } from '../../commons/modal.component';
import { Router } from '@angular/router';
import { URL_CONSTANT } from '../../commons/constants';
import { ItemDto, StatusResultDto } from 'src/app/admin/services/webblog.dto';

@Component({
    templateUrl: './item-list.component.html',
})
export class ItemListComponent implements OnInit {

  statusResult: StatusResultDto;
  templateSelectList: SelectList;
  selectTemplateForm: FormGroup;
  itemsList: Array<ItemDto>;

  @ViewChild('addNewModal', {static: false}) addNewModal: ModalComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;

  constructor(private webblogService: WebBlogService,
    private formBuilder: FormBuilder,
    private router: Router){
      this.selectTemplateForm = this.createTemplateForm();
  }

  async ngOnInit() {
    let templates = await this.webblogService.getTemplates();
    this.templateSelectList = new SelectList(templates, 'name', 'id');
    this.itemsList = await this.webblogService.getItems(null, true);
  }

  createTemplateForm(): FormGroup {
    return this.formBuilder.group({
      template: [null, Validators.required]
    })
  }

  onAddNewClick(): void {
    this.addNewModal.showModal(() => {
      if (this.selectTemplateForm.valid) {
        this.addNewModal.closeModal();
        let selectedId = this.selectTemplateForm.controls.template.value;
        this.router.navigate([URL_CONSTANT.Item.PATH, URL_CONSTANT.Item.ADD_URL, { templateId: selectedId }])
      }
      else {
        this.selectTemplateForm.markAllAsTouched();
      }
    });
  }

  onRemoveItem(item: ItemDto){
    this.confirmModal.showModal(async (item: ItemDto) => {
      
      this.statusResult = await this.webblogService.removeItem(item.id);
      if (this.statusResult.isSuccess) {
        let removeFunc = function removeItems(arrs: ItemDto[], target: ItemDto) {
          let index = arrs.findIndex(x => x.id == target.id);
          if (index != -1) {
            arrs = arrs.splice(index, 1);
          }
          else {
            arrs.forEach(x => {
              removeItems(x.childItemsList, target);
            })
          }
        };
        removeFunc(this.itemsList, item);
      }

      this.confirmModal.closeModal();
    }, item);
  }
}


