import { Component, OnInit } from '@angular/core';
import { WebBlogService } from 'src/app/admin/services/webblog.service';
import { TemplateDto, StatusResultDto, FieldDto } from 'src/app/admin/services/webblog.dto';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { SelectList } from '../../commons/select.component';
import { ActivatedRoute, Router } from '@angular/router';
import { URL_CONSTANT, ENTITY_CONSTANT } from '../../commons/constants';
import { DtoHelper } from 'src/app/admin/services/dto.helper';

@Component({
  templateUrl: './template-create.component.html',
  styles: [`
    .required::after {
      content: " *";
      color: red
    }
    .removeLink i{
      font-size: 20px;
      color: #ff000082;
      cursor: pointer;
    }
    .removeLink:hover i{
      color: red
    }
  `]
})
export class TemplateCreateComponent implements OnInit {
  
  fieldTypeSelectList: SelectList;
  dataSourceSelectList: SelectList;
  
  templateForm: FormGroup;
  fieldsList: FormArray;
  canModify: boolean = true;

  statusResult: StatusResultDto;

  constructor(private webblogService: WebBlogService, 
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) {
  }

  async ngOnInit() {
    let currentPath = this.route.snapshot.routeConfig.path;
    let template: TemplateDto;
    if (currentPath == URL_CONSTANT.Template.EDIT_URL){
      let templateId;
      this.route.paramMap.subscribe(params => {
        templateId = params.get('id');
      });
      
      this.canModify = templateId != ENTITY_CONSTANT.Template.FOLDER_TEMPLATE;
      if (!this.canModify){
        this.statusResult = {
          isSuccess: false,
          message: 'This is standard template, you can not modify!'
        }
      }

      try {
        template = await this.webblogService.getTemplateById(templateId);
      } catch (error) {
        // should redirect 404 page not found.
      }
    } 
    else {
      template = new TemplateDto();
    }
    
    let fieldTypes = await this.webblogService.getFieldTypes();
    this.fieldTypeSelectList = new SelectList(fieldTypes, 'altName', 'id');
    
    let itemDescendant = await this.webblogService.getItems(null, true);
    let selectItemLists = DtoHelper.GetItemHierarchySelectList(itemDescendant);
    this.dataSourceSelectList = new SelectList(selectItemLists, 'text', 'value');

    this.templateForm = this.createTemplateForm(template);
    this.fieldsList = this.templateForm.get('fieldsList') as FormArray;
  }

  private createFieldForms(fields?: Array<FieldDto>): Array<FormGroup> {
    let fieldGroups = [];
    fields.forEach(item => {
      let group = this.formBuilder.group({
        id: item.id,
        name: [item.name, Validators.required],
        fieldTypeId: [item.fieldTypeId, Validators.required],
        isRequired: item.isRequired,
        dataSource: item.dataSource,
      });
      fieldGroups.push(group);
    });
    
    return fieldGroups;
  }

  private shouldShowDatasource(fieldTypeId: string): boolean {
    return fieldTypeId == ENTITY_CONSTANT.FieldType.ITEM_REFERENCE || fieldTypeId == ENTITY_CONSTANT.FieldType.ITEM_SET_REFERENCE;
  }

  private createTemplateForm(template?: TemplateDto): FormGroup {
    if (template == null)
      template = new TemplateDto();

    return this.formBuilder.group({
      name: [template.name, Validators.required],
      id: template.id,
      createdOn: template.createdOn,
      updatedOn: template.updatedOn,
      fieldsList: this.formBuilder.array(this.createFieldForms(template.fieldsList)),
    });
  }

  addField(): void {
    let newField = this.createFieldForms([new FieldDto()]).pop();
    this.fieldsList.push(newField);
  }

  removeField(index: number): void {
    this.fieldsList.removeAt(index);
  }

  async onSubmit(): Promise<void> {
    if (this.templateForm.valid) {
      
      let template = new TemplateDto(this.templateForm.value);
      this.statusResult = await this.webblogService.insertOrUpdateTemplates(template);
      
      if (this.statusResult.isSuccess){
        this.router.navigate([URL_CONSTANT.Template.PATH, URL_CONSTANT.Template.LIST_URL])
      }
    } else {
      this.templateForm.markAllAsTouched();
    }
  }
}