import { Component, OnInit, ViewChild } from '@angular/core';
import { StatusResultDto, TemplateDto, ItemDto, ItemValueDto, ImageDto, PagedList } from 'src/app/admin/services/webblog.dto';
import { WebBlogService } from 'src/app/admin/services/webblog.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { URL_CONSTANT, ENTITY_CONSTANT, FormElement } from '../../commons/constants';
import { SelectList } from '../../commons/select.component';
import { plainToClass } from 'class-transformer';
import { DtoHelper } from 'src/app/admin/services/dto.helper';
import { ModalComponent } from '../../commons/modal.component';


@Component({
    templateUrl: './item-editor.component.html',
})
export class ItemEditorComponent implements OnInit {
    FormElement = FormElement;
    
    itemForm: FormGroup;
    statusResult: StatusResultDto;
    itemSelectList: SelectList;
    template: TemplateDto;

    imagePickerForm: FormGroup;

    @ViewChild('imageModal', {static: false}) imageModal: ModalComponent;
    imagePagedList: PagedList<ImageDto>;

    constructor(private webblogService: WebBlogService, 
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router) {
    }
    
    async ngOnInit() {
        let urlParams;
        this.route.paramMap.subscribe(params => {
            urlParams = params;
        });

        let currentPath = this.route.snapshot.routeConfig.path;
        let item: ItemDto;
        if (currentPath == URL_CONSTANT.Item.ADD_URL){
            let templateId = urlParams.get('templateId');
            this.template = await this.webblogService.getTemplateById(templateId);
            item = new ItemDto();
        }
        else {
            let itemId = urlParams.get('id');
            item = await this.webblogService.getItemById(itemId);
            this.template = await this.webblogService.getTemplateById(item.templateId);
        }
        this.itemForm = this.createItemForm(this.template, item);
        
        let itemDescendant = await this.webblogService.getItems(null, true);
        let selectItemLists = DtoHelper.GetItemHierarchySelectList(itemDescendant);
        this.itemSelectList = new SelectList(selectItemLists, 'text', 'value');

        await this.loadImages();
        this.imagePickerForm = this.formBuilder.group({
            selectedImage: [null, Validators.required]
        });
    }

    createItemForm(template: TemplateDto, item: ItemDto): FormGroup {
        let fieldFormGroups: FormGroup[] = [];
        
        template.fieldsList.forEach(field => {
            
            let itemValue = new ItemValueDto();
            if (item.itemValuesList != null) {
                itemValue = item.itemValuesList
                                .find(x => x.fieldId == field.id) || new ItemValueDto();
            }

            let formGroup = this.formBuilder.group({
                id: [itemValue.id],
                value: [itemValue.value, field.isRequired ? Validators.required : null],
                fieldId: [field.id]
            })
            fieldFormGroups.push(formGroup);
        });

        return this.formBuilder.group({
            id: [item.id],
            name: [item.name, Validators.required],
            parentItemId: [item.parentItemId],
            templateId: [template.id],
            itemValuesList: this.formBuilder.array(fieldFormGroups)
        });
    }

    async onSubmit(){
        if (this.itemForm.valid) {
           
            let item = plainToClass(ItemDto, this.itemForm.value);
            item.itemValuesList = item.itemValuesList.filter(x => x.id != null || x.value != null);
            
            this.statusResult = await this.webblogService.insertOrUpdateItem(item);
            if (this.statusResult.isSuccess) {
                this.router.navigate([URL_CONSTANT.Item.PATH, URL_CONSTANT.Item.LIST_URL])
            }
          } 
          else {
            this.itemForm.markAllAsTouched();
        }
    }

    getFieldInfo(fieldId: string, template: TemplateDto) {
        return template.fieldsList.find(x => x.id == fieldId);
    }

    getFormElement(value: string): FormElement {
        let fieldType = ENTITY_CONSTANT.FieldType; 
        let formElement: FormElement;

        switch(value) {
            case fieldType.ITEM_REFERENCE:
                formElement = FormElement.Select;
                break;
            case fieldType.ITEM_SET_REFERENCE:
                formElement = FormElement.MultipleSelect;
                break;
            case fieldType.PLAIN_TEXT:
                formElement = FormElement.PlainText;    
                break;
            case fieldType.RICH_TEXT:
                formElement = FormElement.RichText;    
                break;
            case fieldType.NUMBER:
                formElement = FormElement.Number;
                break;
            case fieldType.BOOL:
                formElement = FormElement.Bool; 
                break;
            case fieldType.DATE_TIME:
                formElement = FormElement.DateTime; 
                break;
            case fieldType.IMAGE_REFERENCE:
                formElement = FormElement.Image; 
                break;
            default:
                break;
        }
        return formElement;
    }

    async getSelectListByDatasource(itemId: string, service: WebBlogService): Promise<SelectList> {
        if (itemId == null || itemId.length == 0)
            return new SelectList();

        let items = await service.getItems(itemId, true);
        
        let selectItemLists = DtoHelper.GetItemHierarchySelectList(items);
        let selectList = new SelectList(selectItemLists, 'text', 'value');

        return selectList;
    }

    async loadImages(page?: number): Promise<void> {
        this.imagePagedList = await this.webblogService.getImages(page);
    }

    openImagePicker(formControl: FormControl){
        this.imageModal.showModal((formCtrl) => {
            
            if (this.imagePickerForm.valid) {
                let selected = this.imagePickerForm.get('selectedImage').value;
                formCtrl.setValue(selected);
                
                this.imageModal.closeModal();
            }
            else {
                this.imagePickerForm.markAllAsTouched();
            }
        }, formControl);
    }
}