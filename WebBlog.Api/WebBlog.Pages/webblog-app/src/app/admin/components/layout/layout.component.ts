import { Component } from '@angular/core';

@Component({
  template: `
    <div class="wrapper skin-blue">
      <app-header></app-header>
      
      <div class="container-fluid">
          <div class="row">
              <app-leftsidebar class="col-md-2"></app-leftsidebar>

              <div class="col-md-10 main">
                  <router-outlet></router-outlet>
              </div>
          </div>
      </div>
    </div>
  `,
  styles: [`
    .main {
      margin-top: 56px;
      padding-top: 10px;
    }
  `]
})
export class LayoutComponent {}
