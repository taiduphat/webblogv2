import { Component } from '@angular/core';
import { trigger } from '@angular/animations';
import { toggleAnimation } from '../animations/animations';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    animations: [
      trigger('toggle', toggleAnimation('200px')),
    ],
})

export class HeaderComponent {
    isCollapse = false;

    constructor(private authService: OAuthService){
    }

    onSignOut() {
        sessionStorage.clear();
        this.authService.logOut();
    }
}
