import { state, style, animate, transition } from '@angular/animations';

export function toggleAnimation(maxHeight: string):any[] 
{
    const toggleAnimation: any[] = [
        state('true', style({
            'max-height': maxHeight,
        })),
        state('false', style({
            'max-height': '0',
        })),
        transition('true <=> false', [ 
            animate('0.25s')
        ])
    ];
    return toggleAnimation;
}

export function hideAnimation(opacity: number):any[] 
{
    const toggleAnimation: any[] = [
        state('true', style({
            'opacity': 0,
        })),
        state('false', style({
            'opacity': opacity,
        })),
        transition('true <=> false', [ 
            animate('0.5s')
        ])
    ];
    return toggleAnimation;
}