import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router, ActivatedRoute } from '@angular/router';
import { URL_CONSTANT } from '../commons/constants';


@Component({
    template: `
    <div *ngIf="hasError" class="row justify-content-center">
        <div class="col-md-8 text-center">
            <div class="alert alert-warning" role="alert">
                Oops, there was an error, please try to <a routerLink="/">login again</a>.
            </div>
        </div>
    </div>
    `
})
export class AuthCallbackComponent implements OnInit {
  
    hasError: boolean;

    constructor(private authService: OAuthService,
        private router: Router, 
        private route: ActivatedRoute) {}
  
    async ngOnInit() {
        let fragment = this.route.snapshot.fragment;
        if (fragment && fragment.indexOf('error') >= 0) {
            this.hasError = true; 
            return;    
        }
        
        this.router.navigate([URL_CONSTANT.Dashboard.PATH])
    }
  }