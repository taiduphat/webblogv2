import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';



@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: OAuthService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    if (this.authService.hasValidAccessToken() && this.authService.hasValidIdToken()) {
      return true;
    } else {
      return this.authService.loadDiscoveryDocumentAndLogin();
    }
  }
}
