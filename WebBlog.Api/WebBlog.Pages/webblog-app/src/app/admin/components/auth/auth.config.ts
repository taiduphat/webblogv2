import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

const appUrl = `${window.location.protocol}//${window.location.host}`;

export const authConfig: AuthConfig = {
  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId: 'angular_spa',
 
  // Url of the Identity Provider
  issuer: environment.authorityUrl,

  tokenEndpoint: `${environment.authorityUrl}/connect/token`,

  // Authorization code flow
  responseType: 'code',

  requireHttps: false,

  // URL of the SPA to redirect the user to after login
  redirectUri: `${appUrl}/auth-callback`,
 
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid profile webblogapi.write webblogapi.read',
}
