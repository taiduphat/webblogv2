import { 
    Component, ViewChild, ElementRef, Directive, HostListener, OnInit
} from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject } from 'rxjs';
import { filter } from "rxjs/operators";

@Component({
    selector: 'app-leftsidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
@Directive({selector: 'navSidebar li.tree-view a'})
export class SidebarComponent implements OnInit {
    
    identityClaims: Subject<object> = new Subject();
    fullName: string;
    @ViewChild('navSidebar', {static: false}) navSidebar: ElementRef;

    constructor(private authService: OAuthService){
    }

    ngOnInit(): void {
        this.identityClaims.subscribe({
            next: claims => this.fullName = claims['fullname']
        })

        this.authService.events
          .pipe(filter((e) => e.type == "user_profile_loaded"))
          .subscribe(() => {
            this.identityClaims.next(this.authService.getIdentityClaims());
        })

        this.identityClaims.next(this.authService.getIdentityClaims());
    }

    @HostListener('click', ['$event']) 
    onClick(event) {
        var parentElement = event.srcElement.parentElement;
        var isActive = parentElement.classList.contains('active');

        // remove all active class
        var matchedElements = this.navSidebar.nativeElement.querySelectorAll('li.tree-view.active');
        matchedElements.forEach(element => element.classList.remove('active'));

         // active link and expand child-menu
        if (!isActive) {
            parentElement.classList.add('active');
        }
    }
}
