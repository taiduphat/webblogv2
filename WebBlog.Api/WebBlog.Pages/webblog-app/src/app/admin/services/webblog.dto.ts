import { Transform, Expose } from 'class-transformer';
import { DtoHelper } from './dto.helper';

export class FieldTypeDto {
    id: string;
    altName: string;
    description: string;
}
export class FieldDto {
    id: string;
    name: string;
    fieldTypeId: string;
    isRequired: boolean;
    dataSource: string;
}
export class TemplateDto {
    id: string;
    name: string;
    slug: string;

    @Transform(value => DtoHelper.GetDatetime(value), { toClassOnly: true })
    createdOn: string;
    
    @Transform(value => DtoHelper.GetDatetime(value), { toClassOnly: true })
    updatedOn: string;

    fieldsList: Array<FieldDto> = [];

    public constructor(init?: Partial<TemplateDto>){
        Object.assign(this, init);
    }
}
export class StatusResultDto {
    isSuccess: boolean;
    message: string;

    public constructor(init?: Partial<StatusResultDto>){
        Object.assign(this, init);
    }
}
export class ImageDto {
    id?: string;
    fileName: string;
    content: Uint8Array;
    isOverwrite: boolean;

    @Transform(value => DtoHelper.GetDatetime(value), { toClassOnly: true })
    createdOn?: string;

    public constructor(init?: Partial<ImageDto>){
        Object.assign(this, init);
    }
}
export class PagedList<T> {
    @Expose() currentPage: number;
    @Expose() pageSize: number;
    @Expose() pageCount: number;
    @Expose() results?: Array<T>;
}


export class ItemValueDto {
    id: string;
    fieldId: string;
    value: any;

    public constructor(init?: Partial<ItemValueDto>){
        Object.assign(this, init);
    }
}

export class ItemDto {
    id: string;
    name: string;
    slug: string;
    templateId: string;
    parentItemId: string;
    itemValuesList: Array<ItemValueDto>;
    childItemsList: Array<ItemDto>;

    public constructor(init?: Partial<ItemDto>){
        Object.assign(this, init);
    }
}