import { Injectable } from '@angular/core';
import { WebBlogRepository } from './webblog.repository';
import { FieldTypeDto, TemplateDto, StatusResultDto, ImageDto, PagedList, ItemDto } from './webblog.dto';
import { plainToClass } from "class-transformer";
import { DtoHelper } from './dto.helper';
import { RemoveFileRequest, GetItemsRequest, ItemResponse, GetItemByIdRequest, RemoveTemplateRequest, RemoveItemRequest } from './generated/webblog_pb';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
    providedIn: 'root',
  })
export class WebBlogService {
    
  webBlogRepository: WebBlogRepository;
  pageSize: number = 12;  
  
  constructor(private authService: OAuthService){
    this.webBlogRepository = new WebBlogRepository(authService);
  }

  async getFieldTypes(): Promise<FieldTypeDto[]> {
      var entities = await this.webBlogRepository.getFieldTypes();
      var result = plainToClass(FieldTypeDto, entities);
      return result;
  }

  async insertOrUpdateTemplates(template: TemplateDto): Promise<StatusResultDto> {
    let request = DtoHelper.GetRequest(template);
    var response = await this.webBlogRepository.insertOrUpdateTemplate(request);
    
    let result = new StatusResultDto(response.status);
    return result;
  }

  async getTemplates(): Promise<TemplateDto[]> {
      var entities = await this.webBlogRepository.getTemplates();
      var result = plainToClass(TemplateDto, entities);
      return result;
  }

  async getTemplateById(id: string): Promise<TemplateDto> {
    var entity = await this.webBlogRepository.getTemplateById(id);
    if (entity.status != null && !entity.status.isSuccess){
      throw new Error(entity.status.message);
    }
    
    var result = plainToClass(TemplateDto, entity.template);
    return result;
  }

  async getImages(page = 1): Promise<PagedList<ImageDto>> {
    var response = await this.webBlogRepository.getUploadImages(page, this.pageSize);
    var result = plainToClass(PagedList, response.pagedList, { excludeExtraneousValues: true }) as PagedList<ImageDto>;
    if (response.filesList.length != 0){
      result.results = plainToClass(ImageDto, response.filesList);
    }
    
    return result;
  }

  async uploadImage(image: ImageDto): Promise<StatusResultDto> {
    let request = DtoHelper.GetUploadFileRequest(image);
    var response = await this.webBlogRepository.uploadImage(request);

    let result = new StatusResultDto(response.status);
    return result;
  }

  async removeImage(imageId): Promise<StatusResultDto> {
    let request = new RemoveFileRequest();
    request.setFileId(imageId);

    var response = await this.webBlogRepository.removeImage(request);
    let result = new StatusResultDto(response.status);
    
    return result;
  }

  async getItems(parentItemId: string, isGetDescendants: boolean = false): Promise<Array<ItemDto>> {
    let request = new GetItemsRequest();
    request.setParentItemId(parentItemId);
    request.setIsGetDescendants(isGetDescendants);

    var response = await this.webBlogRepository.getItems(request);
    var result = plainToClass(ItemDto, response.itemsList);
    return result;
  }

  async insertOrUpdateItem(item: ItemDto): Promise<StatusResultDto> {
    let request = DtoHelper.GetInsertOrUpdateItemRequest(item);
    var response = await this.webBlogRepository.insertOrUpdateItem(request);
    
    let result = new StatusResultDto(response.status);
    return result;
  }

  async getItemById(itemId: string): Promise<ItemDto> {
    let request = new GetItemByIdRequest();
    request.setItemId(itemId);

    var response = await this.webBlogRepository.getItemById(request);
    var result = plainToClass(ItemDto, response.item);
    return result;
  }

  async removeTemplate(templateId: string): Promise<StatusResultDto> {
    let request = new RemoveTemplateRequest();
    request.setTemplateId(templateId);

    var response = await this.webBlogRepository.removeTemplate(request);
    let result = new StatusResultDto(response.status);
    return result;
  }

  async removeItem(itemId: string): Promise<StatusResultDto> {
    let request = new RemoveItemRequest();
    request.setItemId(itemId);

    var response = await this.webBlogRepository.removeItem(request);
    let result = new StatusResultDto(response.status);
    return result;
  }
}


