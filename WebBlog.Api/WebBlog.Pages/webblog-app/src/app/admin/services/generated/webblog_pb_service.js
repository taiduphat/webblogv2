// package: 
// file: webblog.proto

var webblog_pb = require("./webblog_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var Webblog = (function () {
  function Webblog() {}
  Webblog.serviceName = "Webblog";
  return Webblog;
}());

Webblog.GetFieldTypes = {
  methodName: "GetFieldTypes",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: google_protobuf_empty_pb.Empty,
  responseType: webblog_pb.GetFieldTypesResponse
};

Webblog.GetFieldsByTemplate = {
  methodName: "GetFieldsByTemplate",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.GetFieldsByTemplateRequest,
  responseType: webblog_pb.GetFieldsByTemplateResponse
};

Webblog.InsertOrUpdateTemplate = {
  methodName: "InsertOrUpdateTemplate",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.InsertOrUpdateTemplateRequest,
  responseType: webblog_pb.InsertOrUpdateTemplateResponse
};

Webblog.GetTemplates = {
  methodName: "GetTemplates",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.GetTemplatesRequest,
  responseType: webblog_pb.GetTemplatesResponse
};

Webblog.GetTemplateById = {
  methodName: "GetTemplateById",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.GetTemplateByIdRequest,
  responseType: webblog_pb.GetTemplateByIdResponse
};

Webblog.InsertOrUpdateItem = {
  methodName: "InsertOrUpdateItem",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.InsertOrUpdateItemRequest,
  responseType: webblog_pb.InsertOrUpdateItemResponse
};

Webblog.GetItems = {
  methodName: "GetItems",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.GetItemsRequest,
  responseType: webblog_pb.GetItemsResponse
};

Webblog.GetItemById = {
  methodName: "GetItemById",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.GetItemByIdRequest,
  responseType: webblog_pb.GetItemByIdResponse
};

Webblog.RemoveFields = {
  methodName: "RemoveFields",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.RemoveFieldsRequest,
  responseType: webblog_pb.BaseResponse
};

Webblog.UploadFile = {
  methodName: "UploadFile",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.UploadFileRequest,
  responseType: webblog_pb.BaseResponse
};

Webblog.GetFiles = {
  methodName: "GetFiles",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.GetFilesRequest,
  responseType: webblog_pb.GetFilesResponse
};

Webblog.RemoveFile = {
  methodName: "RemoveFile",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.RemoveFileRequest,
  responseType: webblog_pb.BaseResponse
};

Webblog.RemoveTemplate = {
  methodName: "RemoveTemplate",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.RemoveTemplateRequest,
  responseType: webblog_pb.BaseResponse
};

Webblog.RemoveItem = {
  methodName: "RemoveItem",
  service: Webblog,
  requestStream: false,
  responseStream: false,
  requestType: webblog_pb.RemoveItemRequest,
  responseType: webblog_pb.BaseResponse
};

exports.Webblog = Webblog;

function WebblogClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

WebblogClient.prototype.getFieldTypes = function getFieldTypes(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetFieldTypes, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.getFieldsByTemplate = function getFieldsByTemplate(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetFieldsByTemplate, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.insertOrUpdateTemplate = function insertOrUpdateTemplate(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.InsertOrUpdateTemplate, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.getTemplates = function getTemplates(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetTemplates, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.getTemplateById = function getTemplateById(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetTemplateById, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.insertOrUpdateItem = function insertOrUpdateItem(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.InsertOrUpdateItem, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.getItems = function getItems(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetItems, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.getItemById = function getItemById(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetItemById, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.removeFields = function removeFields(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.RemoveFields, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.uploadFile = function uploadFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.UploadFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.getFiles = function getFiles(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.GetFiles, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.removeFile = function removeFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.RemoveFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.removeTemplate = function removeTemplate(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.RemoveTemplate, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebblogClient.prototype.removeItem = function removeItem(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Webblog.RemoveItem, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.WebblogClient = WebblogClient;

