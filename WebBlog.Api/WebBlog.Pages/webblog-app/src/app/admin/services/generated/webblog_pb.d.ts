// package: 
// file: webblog.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class StatusResult extends jspb.Message {
  getIsSuccess(): boolean;
  setIsSuccess(value: boolean): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusResult.AsObject;
  static toObject(includeInstance: boolean, msg: StatusResult): StatusResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StatusResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusResult;
  static deserializeBinaryFromReader(message: StatusResult, reader: jspb.BinaryReader): StatusResult;
}

export namespace StatusResult {
  export type AsObject = {
    isSuccess: boolean,
    message: string,
  }
}

export class BaseResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BaseResponse.AsObject;
  static toObject(includeInstance: boolean, msg: BaseResponse): BaseResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BaseResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BaseResponse;
  static deserializeBinaryFromReader(message: BaseResponse, reader: jspb.BinaryReader): BaseResponse;
}

export namespace BaseResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
  }
}

export class GetFieldTypesResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  clearFieldTypesList(): void;
  getFieldTypesList(): Array<FieldType>;
  setFieldTypesList(value: Array<FieldType>): void;
  addFieldTypes(value?: FieldType, index?: number): FieldType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFieldTypesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFieldTypesResponse): GetFieldTypesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFieldTypesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFieldTypesResponse;
  static deserializeBinaryFromReader(message: GetFieldTypesResponse, reader: jspb.BinaryReader): GetFieldTypesResponse;
}

export namespace GetFieldTypesResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    fieldTypesList: Array<FieldType.AsObject>,
  }
}

export class FieldType extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getAltName(): string;
  setAltName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FieldType.AsObject;
  static toObject(includeInstance: boolean, msg: FieldType): FieldType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FieldType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FieldType;
  static deserializeBinaryFromReader(message: FieldType, reader: jspb.BinaryReader): FieldType;
}

export namespace FieldType {
  export type AsObject = {
    id: string,
    altName: string,
    description: string,
  }
}

export class GetFieldsByTemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFieldsByTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFieldsByTemplateRequest): GetFieldsByTemplateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFieldsByTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFieldsByTemplateRequest;
  static deserializeBinaryFromReader(message: GetFieldsByTemplateRequest, reader: jspb.BinaryReader): GetFieldsByTemplateRequest;
}

export namespace GetFieldsByTemplateRequest {
  export type AsObject = {
    templateId: string,
  }
}

export class GetFieldsByTemplateResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  clearFieldsList(): void;
  getFieldsList(): Array<GetFieldsByTemplateResponse.FieldResponse>;
  setFieldsList(value: Array<GetFieldsByTemplateResponse.FieldResponse>): void;
  addFields(value?: GetFieldsByTemplateResponse.FieldResponse, index?: number): GetFieldsByTemplateResponse.FieldResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFieldsByTemplateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFieldsByTemplateResponse): GetFieldsByTemplateResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFieldsByTemplateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFieldsByTemplateResponse;
  static deserializeBinaryFromReader(message: GetFieldsByTemplateResponse, reader: jspb.BinaryReader): GetFieldsByTemplateResponse;
}

export namespace GetFieldsByTemplateResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    fieldsList: Array<GetFieldsByTemplateResponse.FieldResponse.AsObject>,
  }

  export class FieldResponse extends jspb.Message {
    getId(): string;
    setId(value: string): void;

    getName(): string;
    setName(value: string): void;

    getFieldTypeId(): string;
    setFieldTypeId(value: string): void;

    getIsRequired(): boolean;
    setIsRequired(value: boolean): void;

    getIsEditable(): boolean;
    setIsEditable(value: boolean): void;

    getDataSource(): string;
    setDataSource(value: string): void;

    hasCreatedOn(): boolean;
    clearCreatedOn(): void;
    getCreatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
    setCreatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

    hasUpdatedOn(): boolean;
    clearUpdatedOn(): void;
    getUpdatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
    setUpdatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FieldResponse.AsObject;
    static toObject(includeInstance: boolean, msg: FieldResponse): FieldResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FieldResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FieldResponse;
    static deserializeBinaryFromReader(message: FieldResponse, reader: jspb.BinaryReader): FieldResponse;
  }

  export namespace FieldResponse {
    export type AsObject = {
      id: string,
      name: string,
      fieldTypeId: string,
      isRequired: boolean,
      isEditable: boolean,
      dataSource: string,
      createdOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
      updatedOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    }
  }
}

export class InsertOrUpdateTemplateRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSlug(): string;
  setSlug(value: string): void;

  clearFieldsList(): void;
  getFieldsList(): Array<FieldRequest>;
  setFieldsList(value: Array<FieldRequest>): void;
  addFields(value?: FieldRequest, index?: number): FieldRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertOrUpdateTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: InsertOrUpdateTemplateRequest): InsertOrUpdateTemplateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsertOrUpdateTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsertOrUpdateTemplateRequest;
  static deserializeBinaryFromReader(message: InsertOrUpdateTemplateRequest, reader: jspb.BinaryReader): InsertOrUpdateTemplateRequest;
}

export namespace InsertOrUpdateTemplateRequest {
  export type AsObject = {
    id: string,
    name: string,
    slug: string,
    fieldsList: Array<FieldRequest.AsObject>,
  }
}

export class FieldRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getFieldTypeId(): string;
  setFieldTypeId(value: string): void;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): void;

  getIsEditable(): boolean;
  setIsEditable(value: boolean): void;

  getDataSource(): string;
  setDataSource(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FieldRequest.AsObject;
  static toObject(includeInstance: boolean, msg: FieldRequest): FieldRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FieldRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FieldRequest;
  static deserializeBinaryFromReader(message: FieldRequest, reader: jspb.BinaryReader): FieldRequest;
}

export namespace FieldRequest {
  export type AsObject = {
    id: string,
    name: string,
    fieldTypeId: string,
    isRequired: boolean,
    isEditable: boolean,
    dataSource: string,
  }
}

export class InsertOrUpdateTemplateResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  getTemplateId(): string;
  setTemplateId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertOrUpdateTemplateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: InsertOrUpdateTemplateResponse): InsertOrUpdateTemplateResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsertOrUpdateTemplateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsertOrUpdateTemplateResponse;
  static deserializeBinaryFromReader(message: InsertOrUpdateTemplateResponse, reader: jspb.BinaryReader): InsertOrUpdateTemplateResponse;
}

export namespace InsertOrUpdateTemplateResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    templateId: string,
  }
}

export class TemplateResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSlug(): string;
  setSlug(value: string): void;

  hasCreatedOn(): boolean;
  clearCreatedOn(): void;
  getCreatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedOn(): boolean;
  clearUpdatedOn(): void;
  getUpdatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  clearFieldsList(): void;
  getFieldsList(): Array<GetFieldsByTemplateResponse.FieldResponse>;
  setFieldsList(value: Array<GetFieldsByTemplateResponse.FieldResponse>): void;
  addFields(value?: GetFieldsByTemplateResponse.FieldResponse, index?: number): GetFieldsByTemplateResponse.FieldResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateResponse): TemplateResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TemplateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateResponse;
  static deserializeBinaryFromReader(message: TemplateResponse, reader: jspb.BinaryReader): TemplateResponse;
}

export namespace TemplateResponse {
  export type AsObject = {
    id: string,
    name: string,
    slug: string,
    createdOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    fieldsList: Array<GetFieldsByTemplateResponse.FieldResponse.AsObject>,
  }
}

export class GetTemplatesRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplatesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplatesRequest): GetTemplatesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTemplatesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplatesRequest;
  static deserializeBinaryFromReader(message: GetTemplatesRequest, reader: jspb.BinaryReader): GetTemplatesRequest;
}

export namespace GetTemplatesRequest {
  export type AsObject = {
  }
}

export class GetTemplatesResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  clearTemplatesList(): void;
  getTemplatesList(): Array<TemplateResponse>;
  setTemplatesList(value: Array<TemplateResponse>): void;
  addTemplates(value?: TemplateResponse, index?: number): TemplateResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplatesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplatesResponse): GetTemplatesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTemplatesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplatesResponse;
  static deserializeBinaryFromReader(message: GetTemplatesResponse, reader: jspb.BinaryReader): GetTemplatesResponse;
}

export namespace GetTemplatesResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    templatesList: Array<TemplateResponse.AsObject>,
  }
}

export class InsertOrUpdateItemRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSlug(): string;
  setSlug(value: string): void;

  getTemplateId(): string;
  setTemplateId(value: string): void;

  getParentItemId(): string;
  setParentItemId(value: string): void;

  clearItemValuesList(): void;
  getItemValuesList(): Array<ItemValueRequest>;
  setItemValuesList(value: Array<ItemValueRequest>): void;
  addItemValues(value?: ItemValueRequest, index?: number): ItemValueRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertOrUpdateItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: InsertOrUpdateItemRequest): InsertOrUpdateItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsertOrUpdateItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsertOrUpdateItemRequest;
  static deserializeBinaryFromReader(message: InsertOrUpdateItemRequest, reader: jspb.BinaryReader): InsertOrUpdateItemRequest;
}

export namespace InsertOrUpdateItemRequest {
  export type AsObject = {
    id: string,
    name: string,
    slug: string,
    templateId: string,
    parentItemId: string,
    itemValuesList: Array<ItemValueRequest.AsObject>,
  }
}

export class ItemValueRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getFieldId(): string;
  setFieldId(value: string): void;

  getValue(): string;
  setValue(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ItemValueRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ItemValueRequest): ItemValueRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ItemValueRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ItemValueRequest;
  static deserializeBinaryFromReader(message: ItemValueRequest, reader: jspb.BinaryReader): ItemValueRequest;
}

export namespace ItemValueRequest {
  export type AsObject = {
    id: string,
    fieldId: string,
    value: string,
  }
}

export class InsertOrUpdateItemResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  getItemId(): string;
  setItemId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertOrUpdateItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: InsertOrUpdateItemResponse): InsertOrUpdateItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsertOrUpdateItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsertOrUpdateItemResponse;
  static deserializeBinaryFromReader(message: InsertOrUpdateItemResponse, reader: jspb.BinaryReader): InsertOrUpdateItemResponse;
}

export namespace InsertOrUpdateItemResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    itemId: string,
  }
}

export class GetItemsRequest extends jspb.Message {
  getParentItemId(): string;
  setParentItemId(value: string): void;

  getIsGetDescendants(): boolean;
  setIsGetDescendants(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetItemsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetItemsRequest): GetItemsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetItemsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetItemsRequest;
  static deserializeBinaryFromReader(message: GetItemsRequest, reader: jspb.BinaryReader): GetItemsRequest;
}

export namespace GetItemsRequest {
  export type AsObject = {
    parentItemId: string,
    isGetDescendants: boolean,
  }
}

export class ItemResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSlug(): string;
  setSlug(value: string): void;

  getTemplateId(): string;
  setTemplateId(value: string): void;

  hasCreatedOn(): boolean;
  clearCreatedOn(): void;
  getCreatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedOn(): boolean;
  clearUpdatedOn(): void;
  getUpdatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  clearItemValuesList(): void;
  getItemValuesList(): Array<ItemValueResponse>;
  setItemValuesList(value: Array<ItemValueResponse>): void;
  addItemValues(value?: ItemValueResponse, index?: number): ItemValueResponse;

  getParentItemId(): string;
  setParentItemId(value: string): void;

  getHasChildren(): boolean;
  setHasChildren(value: boolean): void;

  clearChildItemsList(): void;
  getChildItemsList(): Array<ItemResponse>;
  setChildItemsList(value: Array<ItemResponse>): void;
  addChildItems(value?: ItemResponse, index?: number): ItemResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ItemResponse): ItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ItemResponse;
  static deserializeBinaryFromReader(message: ItemResponse, reader: jspb.BinaryReader): ItemResponse;
}

export namespace ItemResponse {
  export type AsObject = {
    id: string,
    name: string,
    slug: string,
    templateId: string,
    createdOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    itemValuesList: Array<ItemValueResponse.AsObject>,
    parentItemId: string,
    hasChildren: boolean,
    childItemsList: Array<ItemResponse.AsObject>,
  }
}

export class ItemValueResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getFieldId(): string;
  setFieldId(value: string): void;

  getFieldName(): string;
  setFieldName(value: string): void;

  getValue(): string;
  setValue(value: string): void;

  hasCreatedOn(): boolean;
  clearCreatedOn(): void;
  getCreatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedOn(): boolean;
  clearUpdatedOn(): void;
  getUpdatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ItemValueResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ItemValueResponse): ItemValueResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ItemValueResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ItemValueResponse;
  static deserializeBinaryFromReader(message: ItemValueResponse, reader: jspb.BinaryReader): ItemValueResponse;
}

export namespace ItemValueResponse {
  export type AsObject = {
    id: string,
    fieldId: string,
    fieldName: string,
    value: string,
    createdOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GetItemsResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  clearItemsList(): void;
  getItemsList(): Array<ItemResponse>;
  setItemsList(value: Array<ItemResponse>): void;
  addItems(value?: ItemResponse, index?: number): ItemResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetItemsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetItemsResponse): GetItemsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetItemsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetItemsResponse;
  static deserializeBinaryFromReader(message: GetItemsResponse, reader: jspb.BinaryReader): GetItemsResponse;
}

export namespace GetItemsResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    itemsList: Array<ItemResponse.AsObject>,
  }
}

export class GetItemByIdRequest extends jspb.Message {
  getItemId(): string;
  setItemId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetItemByIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetItemByIdRequest): GetItemByIdRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetItemByIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetItemByIdRequest;
  static deserializeBinaryFromReader(message: GetItemByIdRequest, reader: jspb.BinaryReader): GetItemByIdRequest;
}

export namespace GetItemByIdRequest {
  export type AsObject = {
    itemId: string,
  }
}

export class GetItemByIdResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  hasItem(): boolean;
  clearItem(): void;
  getItem(): ItemResponse | undefined;
  setItem(value?: ItemResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetItemByIdResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetItemByIdResponse): GetItemByIdResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetItemByIdResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetItemByIdResponse;
  static deserializeBinaryFromReader(message: GetItemByIdResponse, reader: jspb.BinaryReader): GetItemByIdResponse;
}

export namespace GetItemByIdResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    item?: ItemResponse.AsObject,
  }
}

export class RemoveFieldsRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): void;

  clearFieldIdsList(): void;
  getFieldIdsList(): Array<string>;
  setFieldIdsList(value: Array<string>): void;
  addFieldIds(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoveFieldsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RemoveFieldsRequest): RemoveFieldsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoveFieldsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoveFieldsRequest;
  static deserializeBinaryFromReader(message: RemoveFieldsRequest, reader: jspb.BinaryReader): RemoveFieldsRequest;
}

export namespace RemoveFieldsRequest {
  export type AsObject = {
    templateId: string,
    fieldIdsList: Array<string>,
  }
}

export class GetTemplateByIdRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateByIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateByIdRequest): GetTemplateByIdRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTemplateByIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateByIdRequest;
  static deserializeBinaryFromReader(message: GetTemplateByIdRequest, reader: jspb.BinaryReader): GetTemplateByIdRequest;
}

export namespace GetTemplateByIdRequest {
  export type AsObject = {
    templateId: string,
  }
}

export class GetTemplateByIdResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  hasTemplate(): boolean;
  clearTemplate(): void;
  getTemplate(): TemplateResponse | undefined;
  setTemplate(value?: TemplateResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateByIdResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateByIdResponse): GetTemplateByIdResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTemplateByIdResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateByIdResponse;
  static deserializeBinaryFromReader(message: GetTemplateByIdResponse, reader: jspb.BinaryReader): GetTemplateByIdResponse;
}

export namespace GetTemplateByIdResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    template?: TemplateResponse.AsObject,
  }
}

export class UploadFileRequest extends jspb.Message {
  getFileName(): string;
  setFileName(value: string): void;

  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  getIsOverwrite(): boolean;
  setIsOverwrite(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UploadFileRequest): UploadFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UploadFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadFileRequest;
  static deserializeBinaryFromReader(message: UploadFileRequest, reader: jspb.BinaryReader): UploadFileRequest;
}

export namespace UploadFileRequest {
  export type AsObject = {
    fileName: string,
    content: Uint8Array | string,
    isOverwrite: boolean,
  }
}

export class FileResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getFileName(): string;
  setFileName(value: string): void;

  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  hasCreatedOn(): boolean;
  clearCreatedOn(): void;
  getCreatedOn(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedOn(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: FileResponse): FileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FileResponse;
  static deserializeBinaryFromReader(message: FileResponse, reader: jspb.BinaryReader): FileResponse;
}

export namespace FileResponse {
  export type AsObject = {
    id: string,
    fileName: string,
    content: Uint8Array | string,
    createdOn?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class PagedList extends jspb.Message {
  getCurrentPage(): number;
  setCurrentPage(value: number): void;

  getPageSize(): number;
  setPageSize(value: number): void;

  getPageCount(): number;
  setPageCount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PagedList.AsObject;
  static toObject(includeInstance: boolean, msg: PagedList): PagedList.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PagedList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PagedList;
  static deserializeBinaryFromReader(message: PagedList, reader: jspb.BinaryReader): PagedList;
}

export namespace PagedList {
  export type AsObject = {
    currentPage: number,
    pageSize: number,
    pageCount: number,
  }
}

export class GetFilesRequest extends jspb.Message {
  getPage(): number;
  setPage(value: number): void;

  getPageSize(): number;
  setPageSize(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFilesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFilesRequest): GetFilesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFilesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFilesRequest;
  static deserializeBinaryFromReader(message: GetFilesRequest, reader: jspb.BinaryReader): GetFilesRequest;
}

export namespace GetFilesRequest {
  export type AsObject = {
    page: number,
    pageSize: number,
  }
}

export class GetFilesResponse extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): StatusResult | undefined;
  setStatus(value?: StatusResult): void;

  clearFilesList(): void;
  getFilesList(): Array<FileResponse>;
  setFilesList(value: Array<FileResponse>): void;
  addFiles(value?: FileResponse, index?: number): FileResponse;

  hasPagedList(): boolean;
  clearPagedList(): void;
  getPagedList(): PagedList | undefined;
  setPagedList(value?: PagedList): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFilesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFilesResponse): GetFilesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFilesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFilesResponse;
  static deserializeBinaryFromReader(message: GetFilesResponse, reader: jspb.BinaryReader): GetFilesResponse;
}

export namespace GetFilesResponse {
  export type AsObject = {
    status?: StatusResult.AsObject,
    filesList: Array<FileResponse.AsObject>,
    pagedList?: PagedList.AsObject,
  }
}

export class RemoveFileRequest extends jspb.Message {
  getFileId(): string;
  setFileId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoveFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RemoveFileRequest): RemoveFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoveFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoveFileRequest;
  static deserializeBinaryFromReader(message: RemoveFileRequest, reader: jspb.BinaryReader): RemoveFileRequest;
}

export namespace RemoveFileRequest {
  export type AsObject = {
    fileId: string,
  }
}

export class RemoveTemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoveTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RemoveTemplateRequest): RemoveTemplateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoveTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoveTemplateRequest;
  static deserializeBinaryFromReader(message: RemoveTemplateRequest, reader: jspb.BinaryReader): RemoveTemplateRequest;
}

export namespace RemoveTemplateRequest {
  export type AsObject = {
    templateId: string,
  }
}

export class RemoveItemRequest extends jspb.Message {
  getItemId(): string;
  setItemId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoveItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RemoveItemRequest): RemoveItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoveItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoveItemRequest;
  static deserializeBinaryFromReader(message: RemoveItemRequest, reader: jspb.BinaryReader): RemoveItemRequest;
}

export namespace RemoveItemRequest {
  export type AsObject = {
    itemId: string,
  }
}

