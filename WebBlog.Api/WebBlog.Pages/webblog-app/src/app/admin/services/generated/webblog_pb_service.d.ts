// package: 
// file: webblog.proto

import * as webblog_pb from "./webblog_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import {grpc} from "@improbable-eng/grpc-web";

type WebblogGetFieldTypes = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof google_protobuf_empty_pb.Empty;
  readonly responseType: typeof webblog_pb.GetFieldTypesResponse;
};

type WebblogGetFieldsByTemplate = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.GetFieldsByTemplateRequest;
  readonly responseType: typeof webblog_pb.GetFieldsByTemplateResponse;
};

type WebblogInsertOrUpdateTemplate = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.InsertOrUpdateTemplateRequest;
  readonly responseType: typeof webblog_pb.InsertOrUpdateTemplateResponse;
};

type WebblogGetTemplates = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.GetTemplatesRequest;
  readonly responseType: typeof webblog_pb.GetTemplatesResponse;
};

type WebblogGetTemplateById = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.GetTemplateByIdRequest;
  readonly responseType: typeof webblog_pb.GetTemplateByIdResponse;
};

type WebblogInsertOrUpdateItem = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.InsertOrUpdateItemRequest;
  readonly responseType: typeof webblog_pb.InsertOrUpdateItemResponse;
};

type WebblogGetItems = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.GetItemsRequest;
  readonly responseType: typeof webblog_pb.GetItemsResponse;
};

type WebblogGetItemById = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.GetItemByIdRequest;
  readonly responseType: typeof webblog_pb.GetItemByIdResponse;
};

type WebblogRemoveFields = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.RemoveFieldsRequest;
  readonly responseType: typeof webblog_pb.BaseResponse;
};

type WebblogUploadFile = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.UploadFileRequest;
  readonly responseType: typeof webblog_pb.BaseResponse;
};

type WebblogGetFiles = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.GetFilesRequest;
  readonly responseType: typeof webblog_pb.GetFilesResponse;
};

type WebblogRemoveFile = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.RemoveFileRequest;
  readonly responseType: typeof webblog_pb.BaseResponse;
};

type WebblogRemoveTemplate = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.RemoveTemplateRequest;
  readonly responseType: typeof webblog_pb.BaseResponse;
};

type WebblogRemoveItem = {
  readonly methodName: string;
  readonly service: typeof Webblog;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof webblog_pb.RemoveItemRequest;
  readonly responseType: typeof webblog_pb.BaseResponse;
};

export class Webblog {
  static readonly serviceName: string;
  static readonly GetFieldTypes: WebblogGetFieldTypes;
  static readonly GetFieldsByTemplate: WebblogGetFieldsByTemplate;
  static readonly InsertOrUpdateTemplate: WebblogInsertOrUpdateTemplate;
  static readonly GetTemplates: WebblogGetTemplates;
  static readonly GetTemplateById: WebblogGetTemplateById;
  static readonly InsertOrUpdateItem: WebblogInsertOrUpdateItem;
  static readonly GetItems: WebblogGetItems;
  static readonly GetItemById: WebblogGetItemById;
  static readonly RemoveFields: WebblogRemoveFields;
  static readonly UploadFile: WebblogUploadFile;
  static readonly GetFiles: WebblogGetFiles;
  static readonly RemoveFile: WebblogRemoveFile;
  static readonly RemoveTemplate: WebblogRemoveTemplate;
  static readonly RemoveItem: WebblogRemoveItem;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class WebblogClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getFieldTypes(
    requestMessage: google_protobuf_empty_pb.Empty,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetFieldTypesResponse|null) => void
  ): UnaryResponse;
  getFieldTypes(
    requestMessage: google_protobuf_empty_pb.Empty,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetFieldTypesResponse|null) => void
  ): UnaryResponse;
  getFieldsByTemplate(
    requestMessage: webblog_pb.GetFieldsByTemplateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetFieldsByTemplateResponse|null) => void
  ): UnaryResponse;
  getFieldsByTemplate(
    requestMessage: webblog_pb.GetFieldsByTemplateRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetFieldsByTemplateResponse|null) => void
  ): UnaryResponse;
  insertOrUpdateTemplate(
    requestMessage: webblog_pb.InsertOrUpdateTemplateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.InsertOrUpdateTemplateResponse|null) => void
  ): UnaryResponse;
  insertOrUpdateTemplate(
    requestMessage: webblog_pb.InsertOrUpdateTemplateRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.InsertOrUpdateTemplateResponse|null) => void
  ): UnaryResponse;
  getTemplates(
    requestMessage: webblog_pb.GetTemplatesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetTemplatesResponse|null) => void
  ): UnaryResponse;
  getTemplates(
    requestMessage: webblog_pb.GetTemplatesRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetTemplatesResponse|null) => void
  ): UnaryResponse;
  getTemplateById(
    requestMessage: webblog_pb.GetTemplateByIdRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetTemplateByIdResponse|null) => void
  ): UnaryResponse;
  getTemplateById(
    requestMessage: webblog_pb.GetTemplateByIdRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetTemplateByIdResponse|null) => void
  ): UnaryResponse;
  insertOrUpdateItem(
    requestMessage: webblog_pb.InsertOrUpdateItemRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.InsertOrUpdateItemResponse|null) => void
  ): UnaryResponse;
  insertOrUpdateItem(
    requestMessage: webblog_pb.InsertOrUpdateItemRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.InsertOrUpdateItemResponse|null) => void
  ): UnaryResponse;
  getItems(
    requestMessage: webblog_pb.GetItemsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetItemsResponse|null) => void
  ): UnaryResponse;
  getItems(
    requestMessage: webblog_pb.GetItemsRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetItemsResponse|null) => void
  ): UnaryResponse;
  getItemById(
    requestMessage: webblog_pb.GetItemByIdRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetItemByIdResponse|null) => void
  ): UnaryResponse;
  getItemById(
    requestMessage: webblog_pb.GetItemByIdRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetItemByIdResponse|null) => void
  ): UnaryResponse;
  removeFields(
    requestMessage: webblog_pb.RemoveFieldsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  removeFields(
    requestMessage: webblog_pb.RemoveFieldsRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  uploadFile(
    requestMessage: webblog_pb.UploadFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  uploadFile(
    requestMessage: webblog_pb.UploadFileRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  getFiles(
    requestMessage: webblog_pb.GetFilesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetFilesResponse|null) => void
  ): UnaryResponse;
  getFiles(
    requestMessage: webblog_pb.GetFilesRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.GetFilesResponse|null) => void
  ): UnaryResponse;
  removeFile(
    requestMessage: webblog_pb.RemoveFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  removeFile(
    requestMessage: webblog_pb.RemoveFileRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  removeTemplate(
    requestMessage: webblog_pb.RemoveTemplateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  removeTemplate(
    requestMessage: webblog_pb.RemoveTemplateRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  removeItem(
    requestMessage: webblog_pb.RemoveItemRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
  removeItem(
    requestMessage: webblog_pb.RemoveItemRequest,
    callback: (error: ServiceError|null, responseMessage: webblog_pb.BaseResponse|null) => void
  ): UnaryResponse;
}

