import { grpc } from "@improbable-eng/grpc-web";
import { environment } from 'src/environments/environment';

import { Webblog } from './generated/webblog_pb_service';
import { 
    FieldType, TemplateResponse,
    
    GetTemplatesResponse, GetTemplatesRequest, InsertOrUpdateTemplateRequest, InsertOrUpdateTemplateResponse, GetTemplateByIdResponse, GetTemplateByIdRequest, GetFilesResponse, GetFilesRequest, UploadFileRequest, BaseResponse, RemoveFileRequest, InsertOrUpdateItemRequest, InsertOrUpdateItemResponse, GetItemsRequest, GetItemsResponse, GetItemByIdRequest, GetItemByIdResponse, RemoveTemplateRequest, RemoveItemRequest
} from './generated/webblog_pb';
import { Empty } from 'google-protobuf/google/protobuf/empty_pb';
import { UnaryRpcOptions } from "@improbable-eng/grpc-web/dist/typings/unary";
import { ProtobufMessage } from '@improbable-eng/grpc-web/dist/typings/message';
import { UnaryMethodDefinition } from '@improbable-eng/grpc-web/dist/typings/service';
import { OAuthService } from 'angular-oauth2-oidc';

const grpcWebProxyHost = environment.apiUrl;

export class WebBlogRepository {
    
    constructor(private authService: OAuthService){
    }

    async getFieldTypes(): Promise<FieldType.AsObject[]> {
        let response = await this.unaryAsync(new Empty(), Webblog.GetFieldTypes);
        return response.toObject().fieldTypesList;
    }

    async insertOrUpdateTemplate(request: InsertOrUpdateTemplateRequest): Promise<InsertOrUpdateTemplateResponse.AsObject> {
        let response = await this.unaryAsync(request, Webblog.InsertOrUpdateTemplate);
        return response.toObject();
    }

    async getTemplates(): Promise<TemplateResponse.AsObject[]> {
        let response = await this.unaryAsync<GetTemplatesResponse>(new GetTemplatesRequest(), Webblog.GetTemplates);
        return response.toObject().templatesList;
    }

    async getTemplateById(id: string): Promise<GetTemplateByIdResponse.AsObject> {
        let request = new GetTemplateByIdRequest();
        request.setTemplateId(id);
        let response = await this.unaryAsync<GetTemplateByIdResponse>(request, Webblog.GetTemplateById);
        
        return response.toObject();
    }

    async getUploadImages(page: number, pageSize: number): Promise<GetFilesResponse.AsObject> {
        let request = new GetFilesRequest();
        request.setPage(page);
        request.setPageSize(pageSize);
        let response = await this.unaryAsync<GetFilesResponse>(request, Webblog.GetFiles);
        
        return response.toObject();
    }

    async uploadImage(request: UploadFileRequest): Promise<BaseResponse.AsObject> {
        let response = await this.unaryAsync<BaseResponse>(request, Webblog.UploadFile);
        return response.toObject()
    }

    async removeImage(request: RemoveFileRequest): Promise<BaseResponse.AsObject> {
        let response = await this.unaryAsync<BaseResponse>(request, Webblog.RemoveFile);
        return response.toObject()
    }

    async insertOrUpdateItem(request: InsertOrUpdateItemRequest): Promise<InsertOrUpdateItemResponse.AsObject> {
        let response = await this.unaryAsync<InsertOrUpdateItemResponse>(request, Webblog.InsertOrUpdateItem);
        return response.toObject()
    }

    async getItems(request: GetItemsRequest): Promise<GetItemsResponse.AsObject> {
        let response = await this.unaryAsync<GetItemsResponse>(request, Webblog.GetItems);
        return response.toObject()
    }

    async getItemById(request: GetItemByIdRequest): Promise<GetItemByIdResponse.AsObject> {
        let response = await this.unaryAsync<GetItemByIdResponse>(request, Webblog.GetItemById);
        return response.toObject()
    }

    async removeTemplate(request: RemoveTemplateRequest): Promise<BaseResponse.AsObject> {
        let response = await this.unaryAsync<BaseResponse>(request, Webblog.RemoveTemplate);
        return response.toObject()
    }

    async removeItem(request: RemoveItemRequest): Promise<BaseResponse.AsObject> {
        let response = await this.unaryAsync<BaseResponse>(request, Webblog.RemoveItem);
        return response.toObject()
    }

    private async unaryAsync<TResponse extends ProtobufMessage>(request: ProtobufMessage, methodDescriptor: UnaryMethodDefinition<ProtobufMessage, TResponse>): Promise<TResponse> {
        let promiseRequest = new Promise<TResponse>((resolve) => {

            let authHeader = {
                'Authorization': `Bearer ${this.authService.getAccessToken()}`
            };

            var unaryOpt: UnaryRpcOptions<ProtobufMessage, TResponse> = {
                request: request,
                metadata: authHeader,
                host: grpcWebProxyHost,
                onEnd: res => {
                    const { status, message } = res;
                    if (status === grpc.Code.OK && message) {
                        resolve(message as TResponse);
                    }
                }
            };
            grpc.unary(methodDescriptor, unaryOpt);    
        });
       
        return promiseRequest;
    }
}


