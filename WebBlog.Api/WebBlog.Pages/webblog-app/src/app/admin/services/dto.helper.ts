import { TemplateDto, ImageDto, ItemDto } from './webblog.dto';
import { InsertOrUpdateTemplateRequest, FieldRequest, UploadFileRequest, InsertOrUpdateItemRequest, ItemValueRequest } from './generated/webblog_pb';
import { Timestamp } from 'google-protobuf/google/protobuf/timestamp_pb';
import { SelectListItem } from '../components/commons/select.component';

export namespace DtoHelper {
  
  export function GetDatetime(timestamp: Timestamp.AsObject){
    if (timestamp == null){
      return null;
    }

    let times = timestamp.seconds * 1e3 + timestamp.nanos / 1e6;
    return new Date(times).toLocaleString();
  }

  export function GetRequest(template: TemplateDto): InsertOrUpdateTemplateRequest {
    
    var request = new InsertOrUpdateTemplateRequest();
    request.setId(template.id);
    request.setName(template.name);

    template.fieldsList.forEach(item => {
      let fieldRequest = new FieldRequest();
      fieldRequest.setId(item.id);
      fieldRequest.setFieldTypeId(item.fieldTypeId);
      fieldRequest.setName(item.name);
      fieldRequest.setDataSource(item.dataSource);
      fieldRequest.setIsRequired(item.isRequired);

      request.addFields(fieldRequest);
    })

    return request;
  }

  export function GetUploadFileRequest(image: ImageDto): UploadFileRequest {
    var request = new UploadFileRequest();
    request.setFileName(image.fileName);
    request.setContent(image.content);
    request.setIsOverwrite(image.isOverwrite);
    
    return request;
  }

  export function GetInsertOrUpdateItemRequest(item: ItemDto): InsertOrUpdateItemRequest {
    var request = new InsertOrUpdateItemRequest();
    request.setId(item.id);
    request.setName(item.name);
    let slug = convertToSlug(item.name);
    request.setSlug(slug);
    request.setParentItemId(item.parentItemId);
    request.setTemplateId(item.templateId);

    if (item.itemValuesList != null) {
      item.itemValuesList.forEach(item => {
        let itemValueRequest = new ItemValueRequest();
        itemValueRequest.setId(item.id);
        itemValueRequest.setFieldId(item.fieldId);
        itemValueRequest.setValue(item.value.toString());
  
        request.addItemValues(itemValueRequest);
      })
    }

    return request;
  }

  export function GetItemHierarchySelectList(items: Array<ItemDto>, prefix = []): SelectListItem[] {
    var selectListItems: SelectListItem[] = [];
    items.forEach(item => {
        let text = prefix.join('') + item.name;
        selectListItems.push(new SelectListItem(text, item.id));
        selectListItems = selectListItems.concat(this.GetItemHierarchySelectList(item.childItemsList, prefix.concat('-')));
    });

    return selectListItems;
  }

  function convertToSlug(text: string)
  {
    return text.toLowerCase()
        .replace(/ /g,'-').replace(/[^\w-]+/g,'');
  }
}
