import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbModule, NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

import { URL_CONSTANT } from './components/commons/constants';
import { LayoutComponent } from './components/layout/layout.component';
import { TemplateListComponent } from './components/content/template/template-list.component';
import { TemplateCreateComponent } from './components/content/template/template-create.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { WebBlogService } from './services/webblog.service';
import { InputComponent } from './components/commons/input.component';
import { SelectComponent } from './components/commons/select.component';
import { ImageManagerComponent } from './components/content/image/image-manager.component';
import { UploadImageComponent } from './components/commons/upload-image.component';
import { PaginationComponent } from './components/commons/pagination.component';
import { ItemListComponent } from './components/content/item/item-list.component';
import { ModalComponent } from './components/commons/modal.component';
import { ItemEditorComponent } from './components/content/item/item-editor.component';
import { CallFuncPipe } from './components/commons/pipe.component';
import { DatetimeComponent } from './components/commons/datetime.component';

const routes: Routes = [
  { 
    path: URL_CONSTANT.Dashboard.PATH, component: LayoutComponent
  },
  { 
    path: URL_CONSTANT.Template.PATH, component: LayoutComponent,
    children: [
      {
        path: URL_CONSTANT.Template.LIST_URL,
        component: TemplateListComponent,
      },
      {
        path: URL_CONSTANT.Template.ADD_URL,
        component: TemplateCreateComponent
      },
      {
        path: URL_CONSTANT.Template.EDIT_URL,
        component: TemplateCreateComponent
      }
    ]
  },
  { 
    path: URL_CONSTANT.Image.PATH, component: LayoutComponent,
    children: [
      {
        path: URL_CONSTANT.Image.LIST_URL,
        component: ImageManagerComponent,
      }
    ]
  },
  { 
    path: URL_CONSTANT.Item.PATH, component: LayoutComponent,
    children: [
      {
        path: URL_CONSTANT.Item.LIST_URL,
        component: ItemListComponent,
      },
      {
        path: URL_CONSTANT.Item.ADD_URL,
        component: ItemEditorComponent
      },
      {
        path: URL_CONSTANT.Item.EDIT_URL,
        component: ItemEditorComponent
      }
    ]
  },
];

@NgModule({
  declarations: [
    HeaderComponent,
    LayoutComponent,
    SidebarComponent,
    TemplateListComponent,
    TemplateCreateComponent,
    InputComponent,
    SelectComponent,
    ImageManagerComponent,
    PaginationComponent,
    UploadImageComponent,
    ItemListComponent,
    ModalComponent,
    ItemEditorComponent,
    CallFuncPipe,
    DatetimeComponent
  ],
  imports: [
    [RouterModule.forChild(routes)],
    CommonModule,
    ReactiveFormsModule,
    CKEditorModule,
    NgbModule,
    FormsModule
  ],
  providers: [WebBlogService, NgbDropdown],
})
export class AdminModule { }
