import { Component } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './admin/components/auth/auth.config';
import { filter } from "rxjs/operators";

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
  title = 'WebBlogPages';
  constructor(private oauthService: OAuthService) {
    this.configure();
  }

  private configure() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();

    // subscribe to token events
    this.oauthService.events
      .pipe(filter((e: any) => e.type === "token_received"))
      .subscribe(() => {
        this.oauthService.getAccessToken();
        this.oauthService.loadUserProfile();
      });
  }
}
