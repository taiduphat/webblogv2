(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["apiUrl"] = `http://localhost:50051`;
  window["env"]["authorityUrl"] = `http://localhost:50055`;
})(this);