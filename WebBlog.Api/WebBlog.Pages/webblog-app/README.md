- RUN GRPC WEB PROXY: Open powershell in webblog-app, then run this cmd

grpcwebproxy\grpcwebproxy 
	--backend_addr=localhost:50051 
	--backend_tls=true 
	--allow_all_origins \
	--server_tls_cert_file=grpcwebproxy\certificate.crt --server_tls_key_file=grpcwebproxy\privateKey.key 
	--backend_tls_noverify
	

