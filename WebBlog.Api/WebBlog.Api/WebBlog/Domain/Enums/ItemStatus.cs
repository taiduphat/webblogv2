﻿namespace WebBlog.Domain.Enums
{
    public enum ItemStatus
    {
        Draft,
        Published,
        Trash
    }
}
