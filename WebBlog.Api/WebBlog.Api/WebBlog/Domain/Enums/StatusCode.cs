﻿namespace WebBlog.Domain.Enums
{
    public enum StatusCode
    {
        Success = 0,
        InternalServerError,
       

        // Invalid
        InvalidFieldTypeValue = 100,
        InvalidFileName = 101,

        // Existed
        TemplateAlreadyExisted = 200,
        TemplateNotMatching,
        ItemValueAlreadyExisted,
        FileNameAlreadyExisted,
        TemplateAlreadyUsed,

        // Not found
        TemplateNotFound = 300,
        FieldNotFound,
        FieldTypeNotFound,
        ItemNotFound,
        ParentItemNotFound,
        ItemValueNotFound,
        FileNotFound,
    }
}
