﻿using System.ComponentModel.DataAnnotations;

namespace WebBlog.Domain.Entities
{
    public class FileStorage : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
