﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebBlog.Domain.Entities
{
    public class Field : BaseEntity
    {
        public Guid TemplateId { get; set; }
        [ForeignKey(nameof(Field.TemplateId))]
        public virtual Template Template { get; set; }

        public Guid FieldTypeId { get; set; }
        [ForeignKey(nameof(Field.FieldTypeId))]
        public virtual FieldType FieldType { get; set; }

        [Required]
        public string Name { get; set; }
        public bool IsRequired { get; set; }
        public bool IsEditable { get; set; }

        public Guid? DataSource { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
