﻿using System.ComponentModel.DataAnnotations;

namespace WebBlog.Domain.Entities
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
