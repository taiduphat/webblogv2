﻿using System.ComponentModel.DataAnnotations;

namespace WebBlog.Domain.Entities
{
    public class Template : BaseEntity
    {
        [Required]
        public string Slug { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public virtual ICollection<Field> Fields { get; set; }

        public Template()
        {
            this.Fields = new HashSet<Field>();
        }

    }
}
