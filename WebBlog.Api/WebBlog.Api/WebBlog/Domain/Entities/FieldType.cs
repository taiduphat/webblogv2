﻿using System.ComponentModel.DataAnnotations;

namespace WebBlog.Domain.Entities
{
    public class FieldType : BaseEntity
    {
        [Required]
        public Enums.FieldType Type { get; set; }
        [Required]
        public string AltName { get; set; }
        public string Description { get; set; }
    }
}
