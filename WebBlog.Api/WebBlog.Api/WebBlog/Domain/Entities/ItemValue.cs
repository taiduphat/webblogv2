﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebBlog.Domain.Entities
{
    public class ItemValue : BaseEntity
    {
        public Guid ItemId { get; set; }
        [ForeignKey(nameof(ItemId))]
        public virtual Item Item { get; set; }

        [Required]
        public string Value { get; set; }
        public Guid FieldId { get; set; }
        [ForeignKey(nameof(FieldId))]
        public virtual Field Field { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
