﻿using AutoMapper;
using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.Items.Queries.GetItems
{
    public class GetItemsQuery : IRequest<ServiceResult<List<ItemDto>>>
    {
        public Guid? ParentItemId { get; set; }
        public bool IsGetDescendants { get; set; }
    }

    public class GetItemsQueryHandler : IRequestHandler<GetItemsQuery, ServiceResult<List<ItemDto>>>
    {
        private readonly IGenericRepository<Item> _itemRepository;
        private readonly IMapper _mapper;

        public GetItemsQueryHandler(IGenericRepository<Item> itemRepository, IMapper mapper)
        {
            _itemRepository = itemRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<ItemDto>>> Handle(GetItemsQuery request, CancellationToken cancellationToken)
        {
            var parentItemId = request.ParentItemId;
            if (request.IsGetDescendants)
            {
                var itemDtos = await GetItemHierarchy(parentItemId);
                return ServiceResult<List<ItemDto>>.Success(itemDtos);
            }
            else
            {
                var matchedItems = await _itemRepository.GetAllByQueryAsync(x => x.ParentItemId == parentItemId);
                var itemIds = matchedItems.Select(x => x.Id).ToList();

                var children = await _itemRepository.GetAllByQueryAsync(x => x.ParentItemId.HasValue && itemIds.Contains(x.ParentItemId.Value));
                var groupResult = matchedItems.GroupJoin(children,
                    left => left.Id,
                    right => right.ParentItemId,
                    (left, right) => new { left, HasChildren = right.Any() });

                var result = new List<ItemDto>();
                foreach (var group in groupResult)
                {
                    var temp = _mapper.Map<ItemDto>(group.left);
                    temp.HasChildren = group.HasChildren;
                    result.Add(temp);
                }

                return ServiceResult<List<ItemDto>>.Success(result);
            }
            throw new NotImplementedException();
        }

        public async Task<List<ItemDto>> GetItemHierarchy(Guid? parentItemId)
        {
            var matchedItems = await _itemRepository
                .GetAllByQueryAsync(x => x.ParentItemId == parentItemId);

            var itemDtos = _mapper.Map<List<ItemDto>>(matchedItems);
            foreach (var item in itemDtos)
            {
                item.ChildItems = await GetItemHierarchy(item.Id);
                item.HasChildren = item.ChildItems.Any();
            }

            return itemDtos;
        }
    }
}
