﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using StatusCode = WebBlog.Domain.Enums.StatusCode;

namespace WebBlog.Application.Items.Queries.GetItemById
{
    public class GetItemByIdQuery : IRequest<ServiceResult<ItemDto>>
    {
        public Guid ItemId { get; set; }
    }

    public class GetItemByIdQueryHandler : IRequestHandler<GetItemByIdQuery, ServiceResult<ItemDto>>
    {
        private readonly IGenericRepository<Item> _itemRepository;
        private readonly IMapper _mapper;

        public GetItemByIdQueryHandler(IGenericRepository<Item> itemRepository, IMapper mapper)
        {
            _itemRepository = itemRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<ItemDto>> Handle(GetItemByIdQuery request, CancellationToken cancellationToken)
        {
            var query = _itemRepository
                .GetQueryable(true)
                .Where(x => x.Id == request.ItemId)
                .Include(x => x.Values)
                .ThenInclude(item => item.Field);

            var item = await query.FirstOrDefaultAsync();
            if (item == null)
                return ServiceResult<ItemDto>.Fail(StatusCode.ItemNotFound);

            return ServiceResult<ItemDto>.Success(_mapper.Map<ItemDto>(item));
        }

        public async Task<List<ItemDto>> GetItemHierarchy(Guid? parentItemId)
        {
            var matchedItems = await _itemRepository
                .GetAllByQueryAsync(x => x.ParentItemId == parentItemId);

            var itemDtos = _mapper.Map<List<ItemDto>>(matchedItems);
            foreach (var item in itemDtos)
            {
                item.ChildItems = await GetItemHierarchy(item.Id);
                item.HasChildren = item.ChildItems.Any();
            }

            return itemDtos;
        }
    }
}
