﻿using MediatR;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using StatusCode = WebBlog.Domain.Enums.StatusCode;

namespace WebBlog.Application.Items.Commands.DeleteItem
{
    public class DeleteItemCommand : IRequest<ServiceResult<bool>>
    {
        public Guid ItemId { get; set; }
    }

    public class DeleteItemCommandHandler : IRequestHandler<DeleteItemCommand, ServiceResult<bool>>
    {
        private readonly IGenericRepository<Item> _itemRepository;

        public DeleteItemCommandHandler(IGenericRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<ServiceResult<bool>> Handle(DeleteItemCommand request, CancellationToken cancellationToken)
        {
            var itemId = request.ItemId;
            bool isExisted = await _itemRepository.ExistsAsync(x => x.Id == itemId);
            if (!isExisted)
            {
                return ServiceResult<bool>.Fail(StatusCode.ItemNotFound);
            }

            var itemIdHirerachy = await GetItemHierarchy(itemId);
            var removeItemIds = new List<Guid> { itemId };
            removeItemIds.AddRange(itemIdHirerachy);

            var entities = await _itemRepository.GetAllByQueryAsync(x => removeItemIds.Contains(x.Id));
            await _itemRepository.DeletesAsync(entities.ToArray());

            return ServiceResult<bool>.Success(true);
        }

        private async Task<IEnumerable<Guid>> GetItemHierarchy(params Guid[] parentItemIds)
        {
            var matchedItems = await _itemRepository
                .GetAllByQueryAsync(x => x.ParentItemId.HasValue && parentItemIds.Contains(x.ParentItemId.Value));

            var matchedIds = matchedItems.Select(x => x.Id).ToArray();
            var childItemIds = await GetItemHierarchy(matchedIds);

            return matchedIds.Concat(childItemIds);
        }
    }
}
