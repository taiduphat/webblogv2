﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using StatusCode = WebBlog.Domain.Enums.StatusCode;

namespace WebBlog.Application.Items.Commands.UpdateItem
{
    public class UpdateItemCommand : IRequest<ServiceResult<Guid>>
    {
        public ItemDto Item { get; set; } = default!;
    }

    public class UpdateItemCommandHandler : IRequestHandler<UpdateItemCommand, ServiceResult<Guid>>
    {
        private readonly IGenericRepository<Item> _itemRepository;
        private readonly IGenericRepository<Field> _fieldRepository;
        private readonly IMapper _mapper;

        public UpdateItemCommandHandler(IGenericRepository<Item> itemRepository, IGenericRepository<Field> fieldRepository, IMapper mapper)
        {
            _itemRepository = itemRepository;
            _fieldRepository = fieldRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<Guid>> Handle(UpdateItemCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var itemDto = request.Item;

                var validateResult = await IsItemValidForUpdate(itemDto);
                if (!validateResult.IsSuccess)
                    return ServiceResult<Guid>.Fail(validateResult.StatusCode);

                var dbItem = _itemRepository.GetQueryable(false)
                    .Include(x => x.Values)
                    .First(x => x.Id == itemDto.Id);

                dbItem.UpdatedOn = DateTime.UtcNow;
                dbItem.Name = itemDto.Name;
                dbItem.Slug = itemDto.Slug;
                dbItem.ParentItemId = itemDto.ParentItemId;

                // update existed item values
                var joinResult = from dbItemValue in dbItem.Values
                                 join itemValue in itemDto.Values on dbItemValue.Id equals itemValue.Id
                                 select (dbItemValue, itemValue);
                foreach ((var dbItemValue, var itemValue) in joinResult)
                {
                    dbItemValue.FieldId = itemValue.FieldId;
                    dbItemValue.Value = itemValue.Value;
                    dbItemValue.UpdatedOn = DateTime.UtcNow;
                }

                // add new item values
                var newValueDtos = itemDto.Values.Where(x => !x.Id.HasValue);
                foreach (var itemValueDto in newValueDtos)
                {
                    var itemValue = _mapper.Map<ItemValue>(itemValueDto);
                    itemValue.Id = Guid.NewGuid();
                    itemValue.CreatedOn = DateTime.UtcNow;

                    dbItem.Values.Add(itemValue);
                }

                // update item
                var result = await _itemRepository.UpdateAsync(dbItem);

                return ServiceResult<Guid>.Success(result.Id);
            }
            catch (Exception)
            {
                return ServiceResult<Guid>.Fail(StatusCode.InternalServerError);
            }
        }

        protected async Task<ServiceResult<bool>> IsItemValidForUpdate(ItemDto itemDto)
        {
            var beforeEdit = _itemRepository.GetQueryable()
                     .Include(x => x.Values)
                     .ThenInclude(x => x.Field)
                     .First(x => x.Id == itemDto.Id);

            if (beforeEdit == null)
                return ServiceResult<bool>.Fail(StatusCode.ItemNotFound);
            else if (beforeEdit.TemplateId != itemDto.TemplateId)
                return ServiceResult<bool>.Fail(StatusCode.TemplateNotMatching);

            // updated fields should be template's fields
            var fieldIds = (await GetFieldsByTemplateId(itemDto.TemplateId))
                .Select(x => x.Id);
            var exceptIds = itemDto.Values
                .Select(x => x.FieldId)
                .Except(fieldIds);
            if (exceptIds.Any())
                return ServiceResult<bool>.Fail(StatusCode.FieldNotFound);

            // updated item value should be existed
            var isExistedValue = !itemDto.Values.Any(x => x.Id.HasValue && !beforeEdit.Values.Any(e => e.Id == x.Id.Value));
            if (!isExistedValue)
                return ServiceResult<bool>.Fail(StatusCode.ItemValueNotFound);

            // The value of fields of this item should have only one value
            var filterFieldIds = beforeEdit.Values
                .Select(x => x.FieldId).ToList();
            isExistedValue = itemDto.Values.Any(x => !x.Id.HasValue && filterFieldIds.Any(fieldId => fieldId == x.FieldId));
            if (isExistedValue)
                return ServiceResult<bool>.Fail(StatusCode.ItemValueAlreadyExisted);

            if (itemDto.ParentItemId.HasValue)
            {
                bool isValidParentItem = await _itemRepository.ExistsAsync(x => x.Id == itemDto.ParentItemId);
                if (!isValidParentItem)
                    return ServiceResult<bool>.Fail(StatusCode.ParentItemNotFound);
            }

            return await IsItemValueValid(itemDto);
        }

        protected async Task<ServiceResult<bool>> IsItemValueValid(ItemDto itemDto)
        {
            var fieldIds = itemDto.Values.Select(x => x.FieldId);
            var dbFields = await GetFieldsByTemplateId(itemDto.TemplateId);

            var allExisted = !fieldIds.Except(dbFields.Select(x => x.Id)).Any();
            if (!allExisted)
                return ServiceResult<bool>.Fail(StatusCode.FieldNotFound);

            return ServiceResult<bool>.Success(true);
        }

        protected async Task<IList<Field>> GetFieldsByTemplateId(Guid templateId)
        {
            var fields = await _fieldRepository
                .GetAllByQueryAsync(x => x.TemplateId == templateId);

            return fields;
        }
    }
}
