﻿using AutoMapper;
using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using StatusCode = WebBlog.Domain.Enums.StatusCode;

namespace WebBlog.Application.Items.Commands.CreateItem
{
    public class CreateItemCommand : IRequest<ServiceResult<Guid>>
    {
        public ItemDto Item { get; set; } = default!;
    }

    public class CreateItemCommandHandler : IRequestHandler<CreateItemCommand, ServiceResult<Guid>>
    {
        private readonly IGenericRepository<Item> _itemRepository;
        private readonly IGenericRepository<Field> _fieldRepository;
        private readonly IMapper _mapper;

        public CreateItemCommandHandler(IGenericRepository<Item> itemRepository, IGenericRepository<Field> fieldRepository, IMapper mapper)
        {
            _itemRepository = itemRepository;
            _fieldRepository = fieldRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<Guid>> Handle(CreateItemCommand request, CancellationToken cancellationToken)
        {
            var itemDto = request.Item;

            var parentItemId = itemDto.ParentItemId;
            if (parentItemId.HasValue)
            {
                bool isValidParentItem = await _itemRepository.ExistsAsync(x => x.Id == parentItemId);
                if (!isValidParentItem)
                    return ServiceResult<Guid>.Fail(StatusCode.ParentItemNotFound);
            }

            var validateInput = await IsItemValueValid(itemDto);
            if (!validateInput.IsSuccess)
                return ServiceResult<Guid>.Fail(validateInput.StatusCode);

            var itemEntity = _mapper.Map<Item>(itemDto);
            itemEntity.Id = Guid.NewGuid();
            itemEntity.CreatedOn = DateTime.UtcNow;
            itemEntity.ItemStatus = Domain.Enums.ItemStatus.Draft;

            var itemValues = new List<ItemValue>();
            foreach (var itemValueDto in itemDto.Values)
            {
                var itemValue = _mapper.Map<ItemValue>(itemValueDto);
                itemValue.Id = Guid.NewGuid();
                itemValue.CreatedOn = DateTime.UtcNow;

                itemValues.Add(itemValue);
            }
                
            itemEntity.Values = itemValues;
            var result = await _itemRepository.InsertAsync(itemEntity);
            
            return ServiceResult<Guid>.Success(result.Id);
        }


        protected async Task<ServiceResult<bool>> IsItemValueValid(ItemDto itemDto)
        {
            var fieldIds = itemDto.Values.Select(x => x.FieldId);
            var dbFields = await _fieldRepository.GetAllByQueryAsync(x => x.TemplateId == itemDto.TemplateId);

            var allExisted = !fieldIds.Except(dbFields.Select(x => x.Id)).Any();
            if (!allExisted)
                return ServiceResult<bool>.Fail(StatusCode.FieldNotFound); 

            return ServiceResult<bool>.Success(true);
        }
    }
}
