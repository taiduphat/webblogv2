﻿using AutoMapper;
using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.FieldTypes.Queries.GetFieldTypes
{
    public class GetFieldTypesQuery : IRequest<ServiceResult<List<FieldTypeDto>>>
    {
    }

    public class GetFieldTypesQueryHandler : IRequestHandler<GetFieldTypesQuery, ServiceResult<List<FieldTypeDto>>>
    {
        private readonly IGenericRepository<FieldType> _fieldTypeRepository;
        private readonly IMapper _mapper;

        public GetFieldTypesQueryHandler(IGenericRepository<FieldType> fieldTypeRepository, IMapper mapper)
        {
            _fieldTypeRepository = fieldTypeRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<FieldTypeDto>>> Handle(GetFieldTypesQuery request, CancellationToken cancellationToken)
        {
            var fieldTypes = await _fieldTypeRepository.GetAllByQueryAsync(x => true);
            var result = _mapper.Map<List<FieldTypeDto>>(fieldTypes);

            return ServiceResult<List<FieldTypeDto>>.Success(result);
        }
    }
}
