﻿using AutoMapper;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FieldTypeDto, FieldType>();
            CreateMap<FieldType, FieldTypeDto>();

            CreateMap<FieldDto, Field>()
                .ForMember(pro => pro.FieldType, opt => opt.Ignore());
            CreateMap<Field, FieldDto>();

            CreateMap<TemplateDto, Template>()
                .ForMember(pro => pro.Fields, opt => opt.Ignore());
            CreateMap<Template, TemplateDto>();

            CreateMap<ItemValueDto, ItemValue>();
            CreateMap<ItemValue, ItemValueDto>();

            CreateMap<ItemDto, Item>()
                .ForMember(pro => pro.Values, opt => opt.Ignore());
            CreateMap<Item, ItemDto>();

            CreateMap(typeof(PagedList<>), typeof(PagedList<>));
            CreateMap<FileStorage, FileStorageDto>()
                .ForMember(pro => pro.FileName, opt => opt.MapFrom(x => x.Name));
        }
    }
}
