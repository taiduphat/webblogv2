﻿using MediatR;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using WebBlog.Domain.Enums;

namespace WebBlog.Application.Files.Commands.DeleteFile
{
    public class DeleteFileCommand : IRequest<ServiceResult<bool>>
    {
        public Guid FileId { get; set; }
    }

    public class DeleteFileCommandHandle : IRequestHandler<DeleteFileCommand, ServiceResult<bool>>
    {
        private readonly IGenericRepository<FileStorage> _fileStorageRepository;
        private readonly IFileStorage _fileStorage;

        public DeleteFileCommandHandle(IGenericRepository<FileStorage> fileStorageRepository, IFileStorage fileStorage)
        {
            _fileStorageRepository = fileStorageRepository;
            _fileStorage = fileStorage;
        }

        public async Task<ServiceResult<bool>> Handle(DeleteFileCommand request, CancellationToken cancellationToken)
        {
            var fileEntity = await _fileStorageRepository.FindByIdAsync(request.FileId);
            if (fileEntity == null)
                return ServiceResult<bool>.Fail(StatusCode.FileNotFound);

            var filePath = _fileStorage.GetFilePath(fileEntity.Id.ToString());
            await _fileStorage.RemoveFile(filePath);

            return ServiceResult<bool>.Success(true);
        }
    }
}
