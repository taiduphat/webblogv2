﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using WebBlog.Domain.Enums;

namespace WebBlog.Application.Files.Commands.UploadFile
{
    public class UploadFileCommand : IRequest<ServiceResult<Guid>>
    {
        public string FileName { get; set; } = default!;
        public byte[] Content { get; set; } = default!;
        public bool ShouldOverwrite { get; set; }
    }

    public class UploadFileCommandHandle : IRequestHandler<UploadFileCommand, ServiceResult<Guid>>
    {
        private readonly IGenericRepository<FileStorage> _fileStorageRepository;
        private readonly IFileStorage _fileStorage;

        public UploadFileCommandHandle(IGenericRepository<FileStorage> fileStorageRepository, IFileStorage fileStorage)
        {
            _fileStorageRepository = fileStorageRepository;
            _fileStorage = fileStorage;
        }

        public async Task<ServiceResult<Guid>> Handle(UploadFileCommand request, CancellationToken cancellationToken)
        {
            string fileName = request.FileName;
            if (string.IsNullOrWhiteSpace(fileName))
                return ServiceResult<Guid>.Fail(StatusCode.InvalidFileName);

            var fileEntity = await _fileStorageRepository
                .GetQueryable()
                .SingleOrDefaultAsync(x => x.Name.Equals(fileName));

            if (fileEntity != null && !request.ShouldOverwrite)
                return ServiceResult<Guid>.Fail(StatusCode.FileNameAlreadyExisted);

            if (fileEntity == null)
            {
                fileEntity = new FileStorage
                {
                    Id = Guid.NewGuid(),
                    CreatedOn = DateTime.UtcNow,
                    Name = fileName
                };
                await _fileStorageRepository.InsertAsync(fileEntity);
            }

            var filename = fileEntity.Id.ToString();
            var filePath = _fileStorage.GetFilePath(filename);
            await _fileStorage.UploadFile(filePath, request.Content);

            return ServiceResult<Guid>.Success(fileEntity.Id);
        }
    }
}
