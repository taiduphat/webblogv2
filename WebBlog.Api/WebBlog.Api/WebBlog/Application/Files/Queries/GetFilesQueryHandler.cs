﻿using AutoMapper;
using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.Files.Queries
{
    public class GetFilesQueryHandler : 
        IRequestHandler<GetFileInfosQuery, ServiceResult<PagedList<FileStorageDto>>>,
        IRequestHandler<GetFileContentQuery, ServiceResult<byte[]>>
    {
        private readonly IGenericRepository<FileStorage> _fileStorageRepository;
        private readonly IFileStorage _fileStorage;
        private readonly IMapper _mapper;

        public GetFilesQueryHandler(IGenericRepository<FileStorage> fileStorageRepository, IFileStorage fileStorage, IMapper mapper)
        {
            _fileStorageRepository = fileStorageRepository;
            _fileStorage = fileStorage;
            _mapper = mapper;
        }

        public async Task<ServiceResult<PagedList<FileStorageDto>>> Handle(GetFileInfosQuery request, CancellationToken cancellationToken)
        {
            var query = _fileStorageRepository.GetQueryable();
            var fileEntities = await _fileStorageRepository.GetListPagedAsync(query, request.Page, request.PageSize);

            var result = _mapper.Map<PagedList<FileStorageDto>>(fileEntities);
            return ServiceResult<PagedList<FileStorageDto>>.Success(result);
        }

        public async Task<ServiceResult<byte[]>> Handle(GetFileContentQuery request, CancellationToken cancellationToken)
        {
            var content = await _fileStorage.GetFileContent(_fileStorage.GetFilePath(request.FileId.ToString()));
            return ServiceResult<byte[]>.Success(content.Result);
        }
    }
}
