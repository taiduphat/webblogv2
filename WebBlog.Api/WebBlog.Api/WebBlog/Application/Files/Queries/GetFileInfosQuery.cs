﻿using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Models;

namespace WebBlog.Application.Files.Queries
{
    public class GetFileInfosQuery : IRequest<ServiceResult<PagedList<FileStorageDto>>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
