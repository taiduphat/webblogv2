﻿using MediatR;
using WebBlog.Application.Common.Models;

namespace WebBlog.Application.Files.Queries
{
    public record GetFileContentQuery(Guid FileId) : IRequest<ServiceResult<byte[]>>;
}
