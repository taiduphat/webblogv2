﻿using MediatR;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using WebBlog.Domain.Enums;

namespace WebBlog.Application.Fields.Commands.DeleteTemplate
{
    public class DeleteTemplateCommand : IRequest<ServiceResult<bool>>
    {
        public Guid TemplateId { get; set; }
    }

    public class DeleteTemplateCommandHandler : IRequestHandler<DeleteTemplateCommand, ServiceResult<bool>>
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IGenericRepository<Item> _itemRepository;

        public DeleteTemplateCommandHandler(IGenericRepository<Template> templateRepository, IGenericRepository<Item> itemRepository)
        {
            _templateRepository = templateRepository;
            _itemRepository = itemRepository;
        }

        public async Task<ServiceResult<bool>> Handle(DeleteTemplateCommand request, CancellationToken cancellationToken)
        {
            var templateId = request.TemplateId;

            var template = await _templateRepository.FindByIdAsync(templateId);
            if (template == null)
            {
                return ServiceResult<bool>.Fail(StatusCode.TemplateNotFound);
            }

            bool isUsing = await _itemRepository.ExistsAsync(x => x.TemplateId == templateId);
            if (isUsing)
            {
                return ServiceResult<bool>.Fail(StatusCode.TemplateAlreadyUsed);
            }

            await _templateRepository.DeletesAsync(template);

            return ServiceResult<bool>.Success(true);
        }
    }
}
