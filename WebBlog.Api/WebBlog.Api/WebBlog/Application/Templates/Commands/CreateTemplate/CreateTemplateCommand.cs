﻿using AutoMapper;
using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using StatusCode = WebBlog.Domain.Enums.StatusCode;

namespace WebBlog.Application.Templates.Commands.CreateTemplate
{
    public class CreateTemplateCommand : IRequest<ServiceResult<Guid>>
    {
        public TemplateDto Template { get; set; } = default!;
    }

    public class CreateTemplateCommandHandler : IRequestHandler<CreateTemplateCommand, ServiceResult<Guid>>
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IGenericRepository<FieldType> _fieldTypeRepository;
        private readonly IMapper _mapper;

        public CreateTemplateCommandHandler(IGenericRepository<Template> templateRepository, IGenericRepository<FieldType> fieldTypeRepository, IMapper mapper)
        {
            _templateRepository = templateRepository;
            _fieldTypeRepository = fieldTypeRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<Guid>> Handle(CreateTemplateCommand request, CancellationToken cancellationToken)
        {
            var templateDto = request.Template;

            bool isExistedTemplate = await _templateRepository.ExistsAsync(x => x.Name.Equals(templateDto.Name));
            if (isExistedTemplate)
                return ServiceResult<Guid>.Fail(StatusCode.TemplateAlreadyExisted);

            var areValidFields = await AreValidFields(templateDto.Fields);
            if (!areValidFields)
                return ServiceResult<Guid>.Fail(StatusCode.FieldTypeNotFound);

            var template = _mapper.Map<Template>(templateDto);
            template.Id = Guid.NewGuid();
            template.CreatedOn = DateTime.UtcNow;

            var templateFields = new List<Field>();
            foreach (var fieldDto in templateDto.Fields)
            {
                var field = _mapper.Map<Field>(fieldDto);
                field.Id = Guid.NewGuid();
                field.TemplateId = template.Id;
                field.CreatedOn = DateTime.UtcNow;

                templateFields.Add(field);
            }       

            template.Fields = templateFields;
            var result = await _templateRepository.InsertAsync(template);

            return ServiceResult<Guid>.Success(result.Id);
        }

        private async Task<bool> AreValidFields(IEnumerable<FieldDto> fieldDtos)
        {
            var fieldTypeIds = fieldDtos.Select(x => x.FieldTypeId);
            var dbFields = await _fieldTypeRepository.GetAllByQueryAsync(x => fieldTypeIds.Contains(x.Id));

            return !fieldTypeIds.Except(dbFields.Select(x => x.Id)).Any();
        }
    }
}
