﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using StatusCode = WebBlog.Domain.Enums.StatusCode;

namespace WebBlog.Application.Templates.Commands.CreateTemplate
{
    public class UpdateTemplateCommand : IRequest<ServiceResult<Guid>>
    {
        public TemplateDto Template { get; set; } = default!;
    }

    public class UpdateTemplateCommandHandler : IRequestHandler<UpdateTemplateCommand, ServiceResult<Guid>>
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IGenericRepository<FieldType> _fieldTypeRepository;
        private readonly IGenericRepository<Field> _fieldRepository;
        private readonly IMapper _mapper;

        public UpdateTemplateCommandHandler(IGenericRepository<Template> templateRepository, 
            IGenericRepository<FieldType> fieldTypeRepository,
            IGenericRepository<Field> fieldRepository, 
            IMapper mapper)
        {
            _templateRepository = templateRepository;
            _fieldTypeRepository = fieldTypeRepository;
            _fieldRepository = fieldRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<Guid>> Handle(UpdateTemplateCommand request, CancellationToken cancellationToken)
        {
            var templateDto = request.Template;

            var dbTemplateEntity = _templateRepository
                .GetQueryable()
                .Include(x => x.Fields)
                .First(x => x.Id == templateDto.Id);

            if (dbTemplateEntity == null)
                return ServiceResult<Guid>.Fail(StatusCode.TemplateNotFound);

            // updated fields should be template's fields
            var fieldIds = templateDto.Fields
                .Where(x => x.Id.HasValue)
                .Select(x => x.Id).ToArray();
            
            var isExisted = fieldIds.Length == 0 || await AreFieldsExisted(templateDto.Id.GetValueOrDefault(), templateDto.Fields);
            if (!isExisted)
                return ServiceResult<Guid>.Fail(StatusCode.FieldNotFound);

            // updated field type should be existed
            isExisted = await AreFieldTypesExisted(templateDto.Fields);
            if (!isExisted)
                return ServiceResult<Guid>.Fail(StatusCode.FieldTypeNotFound);


            try
            {
                var dbFieldEntities = dbTemplateEntity.Fields;

                dbTemplateEntity.UpdatedOn = DateTime.UtcNow;
                dbTemplateEntity.Name = templateDto.Name;
                dbTemplateEntity.Slug = templateDto.Slug;

                // update existed item values
                var joinResult = from dbField in dbFieldEntities
                                 join requestField in templateDto.Fields on dbField.Id equals requestField.Id
                                 select (dbField, requestField);
                
                foreach ((var dbField, var requestField) in joinResult)
                {
                    dbField.FieldTypeId = requestField.FieldTypeId;
                    dbField.Name = requestField.Name;
                    dbField.UpdatedOn = DateTime.UtcNow;
                    dbField.IsRequired = requestField.IsRequired;
                    dbField.DataSource = requestField.DataSource;
                }

                // remove item
                var removeEntities = dbFieldEntities.Where(x => !fieldIds.Contains(x.Id));
                foreach (var item in removeEntities)
                {
                    dbTemplateEntity.Fields.Remove(item);
                }

                // update item
                var result = await _templateRepository.UpdateAsync(dbTemplateEntity);

                // add new item values
                var addFields = templateDto.Fields
                    .Where(x => !x.Id.HasValue)
                    .Select(fieldDto => {
                        var field = _mapper.Map<Field>(fieldDto);
                        field.Id = Guid.NewGuid();
                        field.TemplateId = dbTemplateEntity.Id;
                        field.CreatedOn = DateTime.UtcNow;

                        return field;
                    })
                    .ToList();

                await _fieldRepository.InsertRangeAsync(addFields);

                return ServiceResult<Guid>.Success(result.Id);
            }
            catch (Exception)
            {
                return ServiceResult<Guid>.Fail(StatusCode.InternalServerError);
            }
        }

        private async Task<bool> AreFieldsExisted(Guid templateId, IEnumerable<FieldDto> fieldDtos)
        {
            var dbFields = await _fieldRepository.GetAllByQueryAsync(x => x.TemplateId == templateId);
            var dbFieldIds = dbFields.Select(x => x.Id);

            var fieldIds = fieldDtos.Select(x => x.Id.GetValueOrDefault());
            var areExistedFields = !fieldIds.Except(dbFieldIds).Any();
                
            return areExistedFields;
        }

        private async Task<bool> AreFieldTypesExisted(IEnumerable<FieldDto> fieldDtos)
        {
            var fieldTypeIds = fieldDtos.Select(x => x.FieldTypeId);
            var dbFields = await _fieldTypeRepository.GetAllByQueryAsync(x => fieldTypeIds.Contains(x.Id));

            return !fieldTypeIds.Except(dbFields.Select(x => x.Id)).Any();
        }
    }
}
