﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.Templates.Queries.GetTemplates
{
    public class GetTemplatesQuery : IRequest<ServiceResult<List<TemplateDto>>>
    {
    }

    public class GetTemplatesQueryHandler : IRequestHandler<GetTemplatesQuery, ServiceResult<List<TemplateDto>>>
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IMapper _mapper;

        public GetTemplatesQueryHandler(IGenericRepository<Template> templateRepository, IMapper mapper)
        {
            _templateRepository = templateRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<TemplateDto>>> Handle(GetTemplatesQuery request, CancellationToken cancellationToken)
        {
            var templateEntities = await _templateRepository
                .GetQueryable()
                .ToListAsync();
            var result = _mapper.Map<List<TemplateDto>>(templateEntities);

            return ServiceResult<List<TemplateDto>>.Success(result);
        }
    }
}
