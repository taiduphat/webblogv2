﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.Templates.Queries.GetTemplates
{
    public class GetTemplateByIdQuery : IRequest<ServiceResult<TemplateDto>>
    {
        public Guid TemplateId { get; set; }
    }

    public class GetTemplateByIdQueryHandler : IRequestHandler<GetTemplateByIdQuery, ServiceResult<TemplateDto>>
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IMapper _mapper;

        public GetTemplateByIdQueryHandler(IGenericRepository<Template> templateRepository, IMapper mapper)
        {
            _templateRepository = templateRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<TemplateDto>> Handle(GetTemplateByIdQuery request, CancellationToken cancellationToken)
        {
            var templateEntity = await _templateRepository
                .GetQueryable()
                .Include(x => x.Fields)
                .SingleAsync(x => request.TemplateId == x.Id);

            var result = _mapper.Map<TemplateDto>(templateEntity);

            return ServiceResult<TemplateDto>.Success(result);
        }
    }
}
