﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using WebBlog.Application.Common.Behaviours;

namespace WebBlog.Application
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services)
        {
            var executingAssembly = Assembly.GetExecutingAssembly();

            services.AddAutoMapper(executingAssembly);
            services.AddMediatR(executingAssembly);

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthorizationBehaviour<,>));
        }
    }
}
