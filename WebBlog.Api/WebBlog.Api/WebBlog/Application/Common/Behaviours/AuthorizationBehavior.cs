﻿using MediatR;
using System.Reflection;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Securities;

namespace WebBlog.Application.Common.Behaviours
{
    public class AuthorizationBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IIdentityService _identityService;

        public AuthorizationBehaviour(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var authorizeAttributes = request.GetType().GetCustomAttributes<HasPermissionAttribute>();

            if (!authorizeAttributes.Any())
                return await next();

            // Permissions-based authorization
            var authorizeAttributesWithPermission = authorizeAttributes.Where(x => !string.IsNullOrWhiteSpace(x.Permission));

            bool isAuthorized = true;
            if (authorizeAttributesWithPermission.Any())
            {
                var requiredPermissions = authorizeAttributesWithPermission.Select(x => x.Permission).ToArray();

                isAuthorized = await _identityService.HasPermissionsAsync(requiredPermissions);

                if (!isAuthorized)
                    throw new UnauthorizedAccessException();
            }

            return await next();
        }
    }
}
