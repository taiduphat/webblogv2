﻿using System.Security.Claims;

namespace WebBlog.Application.Common.Interfaces
{
    public interface ICurrentUserService
    {
        public string? UserId { get; }

        public ClaimsIdentity ClaimsIdentity { get; }
    }
}
