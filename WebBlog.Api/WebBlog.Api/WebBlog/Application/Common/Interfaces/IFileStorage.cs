﻿using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Models;

namespace WebBlog.Application.Common.Interfaces
{
    public interface IFileStorage
    {
        Task UploadFile(string filePath, byte[] content);
        Task<ServiceResult<bool>> RemoveFile(string filePath);
        Task<ServiceResult<byte[]>> GetFileContent(string filePath);
        string GetFilePath(string fileName);
    }
}
