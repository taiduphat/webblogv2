﻿using Microsoft.EntityFrameworkCore;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        public DbSet<Field> Fields { get; set; }
        public DbSet<FieldType> FieldTypes { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemValue> ItemValues { get; set; }
        public DbSet<FileStorage> FileStorages { get; set; }
    }
}
