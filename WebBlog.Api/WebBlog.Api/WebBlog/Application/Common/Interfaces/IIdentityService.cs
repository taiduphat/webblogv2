﻿namespace WebBlog.Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<bool> HasPermissionsAsync(string[] permissions);
    }
}
