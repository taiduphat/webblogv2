﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Linq.Expressions;
using WebBlog.Application.Common.Models;

namespace WebBlog.Application.Common.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<TEntity> InsertAsync(TEntity entity);
        Task<List<TEntity>> InsertRangeAsync(List<TEntity> entities);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task DeletesAsync(params TEntity[] entities);
        Task<TEntity> FindByIdAsync(Guid Id);
        IQueryable<TEntity> GetQueryable(bool noTracking = true);
        Task<IList<TEntity>> GetAllByQueryAsync(Expression<Func<TEntity, bool>> expression);
        Task<PagedList<TEntity>> GetListPagedAsync(IQueryable<TEntity> query, int page, int pageSize);
        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
