﻿namespace WebBlog.Application.Common.Securities
{
    /// <summary>
    /// Specifies the class this attribute is applied to requires authorization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class HasPermissionAttribute : Attribute
    {
        public HasPermissionAttribute(string permission) 
        {
            Permission = permission;
        }
        public string Permission { get; set; } = default!;
    }
}
