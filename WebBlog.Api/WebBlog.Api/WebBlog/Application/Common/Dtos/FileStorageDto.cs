﻿namespace WebBlog.Application.Common.Dtos
{
    public class FileStorageDto
    {
        public Guid? Id { get; set; }
        public string FileName { get; set; } = default!;
        public DateTime CreatedOn { get; set; }
        public byte[] Content { get; set; } = default!;
    }
}
