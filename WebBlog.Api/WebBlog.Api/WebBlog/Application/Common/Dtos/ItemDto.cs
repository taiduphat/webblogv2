﻿namespace WebBlog.Application.Common.Dtos
{
    public class ItemDto
    {
        public Guid? Id { get; set; }
        public Guid TemplateId { get; set; }
        public Guid? ParentItemId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Name { get; set; } = default!;
        public string Slug { get; set; } = default!;
        public DateTime? PublishedOn { get; set; }
        public bool HasChildren { get; set; }
        public virtual IList<ItemValueDto> Values { get; set; } = default!;
        public IList<ItemDto> ChildItems { get; set; } = default!;
    }
}
