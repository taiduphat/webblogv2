﻿using MediatR;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;

namespace WebBlog.Application.Fields.Commands.DeleteFields
{
    public class DeleteFieldsByTemplateIdCommand : IRequest<ServiceResult<bool>>
    {
        public Guid TemplateId { get; set; }
        public Guid[] RemoveFieldIds { get; set; } = default!;
    }

    public class DeleteFieldsByTemplateIdCommandHandler : IRequestHandler<DeleteFieldsByTemplateIdCommand, ServiceResult<bool>>
    {
        private readonly IGenericRepository<Field> _fieldRepository;

        public DeleteFieldsByTemplateIdCommandHandler(IGenericRepository<Field> fieldRepository)
        {
            _fieldRepository = fieldRepository;
        }

        public async Task<ServiceResult<bool>> Handle(DeleteFieldsByTemplateIdCommand request, CancellationToken cancellationToken)
        {
            var removedEntities = await _fieldRepository.GetAllByQueryAsync(x => request.RemoveFieldIds.Contains(x.Id));
            await _fieldRepository.DeletesAsync(removedEntities.ToArray());

            return ServiceResult<bool>.Success(true);
        }
    }
}
