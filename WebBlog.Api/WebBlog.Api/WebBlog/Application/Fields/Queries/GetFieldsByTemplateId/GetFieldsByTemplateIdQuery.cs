﻿using AutoMapper;
using MediatR;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain.Entities;
using WebBlog.Domain.Enums;

namespace WebBlog.Application.Fields.Queries.GetFieldsByTemplateId
{
    public class GetFieldsByTemplateIdQuery : IRequest<ServiceResult<List<FieldDto>>>
    {
        public Guid TemplateId { get; set; }
    }

    public class GetFieldsByTemplateIdQueryHandler : IRequestHandler<GetFieldsByTemplateIdQuery, ServiceResult<List<FieldDto>>>
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IGenericRepository<Field> _fieldRepository;
        private readonly IMapper _mapper;

        public GetFieldsByTemplateIdQueryHandler(IGenericRepository<Template> templateRepository, IGenericRepository<Field> fieldRepository, IMapper mapper)
        {
            _templateRepository = templateRepository;
            _fieldRepository = fieldRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<FieldDto>>> Handle(GetFieldsByTemplateIdQuery request, CancellationToken cancellationToken)
        {
            var templateId = request.TemplateId;

            bool existed = await _templateRepository.ExistsAsync(x => x.Id == templateId);
            if (!existed)
            {
                return ServiceResult<List<FieldDto>>.Fail(StatusCode.TemplateNotFound);
            }

            var fields = await _fieldRepository.GetAllByQueryAsync(x => x.TemplateId == templateId);
            var result = _mapper.Map<List<FieldDto>>(fields);

            return ServiceResult<List<FieldDto>>.Success(result);
        }
    }
}
