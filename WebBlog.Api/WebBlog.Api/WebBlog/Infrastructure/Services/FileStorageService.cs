﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;

namespace WebBlog.Infrastructure.Services
{
    public class FileStorageService : IFileStorage
    {
        private readonly string FileStorageLocation;

        public FileStorageService(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            FileStorageLocation = Path.Combine(hostingEnvironment.WebRootPath, configuration.GetValue<string>("FileStorageLocation"));
            if (!Directory.Exists(FileStorageLocation))
            {
                Directory.CreateDirectory(FileStorageLocation);
            }
        }

        public Task<ServiceResult<bool>> RemoveFile(string filePath)
        {
            File.Delete(filePath);
            return Task.FromResult(ServiceResult<bool>.Success(true));
        }

        public async Task UploadFile(string filePath, byte[] content)
        {
            //Create the file.
            using (FileStream fileStream = File.Create(filePath))
            {
                await fileStream.WriteAsync(content).ConfigureAwait(false);
            }

            await Task.CompletedTask;
        }

        public string GetFilePath(string fileName) => Path.Combine(FileStorageLocation, fileName);

        public async Task<ServiceResult<byte[]>> GetFileContent(string filePath)
        {
            var content = await File.ReadAllBytesAsync(filePath);
            return ServiceResult<byte[]>.Success(content);
        }
    }
}
