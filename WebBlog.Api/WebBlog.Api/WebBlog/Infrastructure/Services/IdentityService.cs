﻿using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Interfaces;

namespace WebBlog.Infrastructure.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly ICurrentUserService _currentUserService;

        public IdentityService(ICurrentUserService currentUserService)
        {
            _currentUserService = currentUserService;
        }

        public Task<bool> HasPermissionsAsync(string[] permissions)
        {
            var claimsIdentity = _currentUserService.ClaimsIdentity;

            var userPermissions = claimsIdentity.FindAll(Domain.Constants.PermissionClaimType)?.Select(x => x.Value);
            if (userPermissions == null || !userPermissions.Any())
                return Task.FromResult(false);
            
            return Task.FromResult(permissions.All(x => userPermissions.Contains(x)));
        }
    }
}
