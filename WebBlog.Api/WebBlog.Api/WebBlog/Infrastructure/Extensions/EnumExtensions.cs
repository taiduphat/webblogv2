﻿using System.ComponentModel;
using System.Reflection;

namespace WebBlog.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        private static Type _typeOfFieldTypeEnum = typeof(Domain.Enums.FieldType);

        public static string GetDescription(this Domain.Enums.FieldType value)
        {
            FieldInfo? fi = _typeOfFieldTypeEnum.GetField(value.ToString());

            var attributes = fi?.GetCustomAttributes<DescriptionAttribute>();
            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }
        public static Guid GetDefaultValue(this Domain.Enums.FieldType value)
        {
            FieldInfo? fi = _typeOfFieldTypeEnum.GetField(value.ToString());

            var attributes = fi?.GetCustomAttributes<DefaultValueAttribute>();
            if (attributes != null && attributes.Any())
            {
                Guid.TryParse(attributes.First().Value?.ToString(), out var result);
                return result;
            }

            return Guid.Empty;
        }
    }
}
