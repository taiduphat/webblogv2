﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebBlog.Infrastructure.Persistence.Migrations
{
    public partial class RemoveFilePathColumnInFileStoragesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "FileStorages");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "FileStorages",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
