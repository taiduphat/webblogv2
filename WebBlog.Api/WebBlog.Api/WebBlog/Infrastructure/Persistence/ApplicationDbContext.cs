﻿using Microsoft.EntityFrameworkCore;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Domain;
using WebBlog.Domain.Entities;
using WebBlog.Infrastructure.Extensions;

namespace WebBlog.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public DbSet<Field> Fields { get; set; } = default!;
        public DbSet<FieldType> FieldTypes { get; set; } = default!;
        public DbSet<Template> Templates { get; set; } = default!;
        public DbSet<Item> Items { get; set; } = default!;
        public DbSet<ItemValue> ItemValues { get; set; } = default!;
        public DbSet<FileStorage> FileStorages { get; set; } = default!;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seeds data
            foreach (var fieldType in (Domain.Enums.FieldType[])System.Enum.GetValues(typeof(Domain.Enums.FieldType)))
            {
                modelBuilder.Entity<FieldType>().HasData(
                    new FieldType { Id = fieldType.GetDefaultValue(), Type = fieldType, AltName = fieldType.ToString(), Description = fieldType.GetDescription() }
                );
            }

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
