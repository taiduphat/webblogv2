﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using WebBlog.Application.Common.Interfaces;
using WebBlog.Application.Common.Models;
using WebBlog.Domain;
using WebBlog.Domain.Entities;

namespace WebBlog.Infrastructure.Persistence
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected ApplicationDbContext _dbContext;
        protected DbSet<TEntity> _dbSet => _dbContext.Set<TEntity>();

        public GenericRepository(IApplicationDbContext dbContext)
        {
            _dbContext = (ApplicationDbContext) dbContext;
        }
        public virtual async Task DeletesAsync(params TEntity[] entities)
        {
            _dbSet.RemoveRange(entities);
            await _dbContext.SaveChangesAsync();
        }

        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            _dbSet.Add(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<List<TEntity>> InsertRangeAsync(List<TEntity> entities)
        {
            _dbSet.AddRange(entities);
            await _dbContext.SaveChangesAsync();

            return entities;
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            try
            {
                _dbContext.Update(entity);
                await _dbContext.SaveChangesAsync();
            }
            catch(DbUpdateConcurrencyException ex)
            {
                foreach(var entry in ex.Entries)
                {
                    // Adding to database if its not exist
                    var databaseValues = entry.GetDatabaseValues();
                    if (databaseValues == null)
                    {
                        // Refresh original values to bypass next concurrency check
                        entry.State = EntityState.Added;
                    }
                }
                await _dbContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<PagedList<TEntity>> GetListPagedAsync(IQueryable<TEntity> query, int page, int pageSize)
        {
            var result = new PagedList<TEntity>();
            result.CurrentPage = page;
            result.PageSize = pageSize;
            result.RowCount = query.Count();

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = await query.Skip(skip).Take(pageSize).ToListAsync();

            return result;
        }

        public virtual IQueryable<TEntity> GetQueryable(bool noTracking = true)
        {
            if (noTracking)
                return _dbSet.AsNoTracking();

            return _dbSet.AsQueryable();
        }

        public async Task<IList<TEntity>> GetAllByQueryAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await _dbSet.Where(expression)
                .AsNoTracking()
                .ToListAsync();
        }

        public virtual async Task<TEntity> FindByIdAsync(Guid Id)
        {
            return await _dbSet.FindAsync(Id);
        }

        public async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.AnyAsync(predicate);
        }
    }
}
