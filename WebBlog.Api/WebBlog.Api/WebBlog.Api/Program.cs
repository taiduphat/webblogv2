using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebBlog.Infrastructure.Persistence;

namespace WebBlog.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHost(args);

            // Create database if not exists
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                var dbContext = services.GetRequiredService<ApplicationDbContext>();

                if (!dbContext.Database.IsInMemory())
                    dbContext.Database.Migrate();
            }

            host.Run();
        }

        public static IHost CreateWebHost(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   webBuilder
                       .UseStartup<Startup>()
                       .UseKestrel(options =>
                       {
                           options.ListenAnyIP(80, x => x.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1AndHttp2);
                           options.ListenAnyIP(50051, x => x.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2);
                       })
                       .ConfigureAppConfiguration((hostContext, builder) =>
                       {
                           builder.AddJsonFile("appsettings.json", true, false);
                           builder.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", true, false);

                           if (hostContext.HostingEnvironment.IsDevelopment())
                               builder.AddUserSecrets<Startup>();

                           builder.AddEnvironmentVariables();
                       });
               });

            return hostBuilder.Build();
        }
    }
}
