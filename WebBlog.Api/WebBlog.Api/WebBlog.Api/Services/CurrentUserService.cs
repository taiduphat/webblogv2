﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using WebBlog.Application.Common.Interfaces;

namespace WebBlog.Api.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string UserId => _httpContextAccessor.HttpContext.User?.FindFirstValue(ClaimTypes.NameIdentifier); 
        

        public ClaimsIdentity ClaimsIdentity => (ClaimsIdentity) _httpContextAccessor.HttpContext.User.Identity;
    }
}
