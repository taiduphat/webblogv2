﻿using Google.Protobuf.WellKnownTypes;
using Grpc.WebBlogService;
using Microsoft.Extensions.Localization;
using System;
using System.Text.RegularExpressions;
using WebBlog.Api.Resources;
using WebBlog.Domain.Enums;

namespace WebBlog.Api.Helper
{
    public static class CommonHelper
    {
        public static readonly string SlugRegex = "^[a-z0-9-]+$";
        public static bool IsMatchedRegex(string value)
        {
            return Regex.Match(value, SlugRegex, RegexOptions.IgnoreCase).Success;
        }
        public static bool IsValidGuid(string guid)
        {
            return Guid.TryParse(guid, out var result) && !result.Equals(Guid.Empty);
        }

        public static Guid? ParseGuid(string guid)
        {
            return !string.IsNullOrEmpty(guid) ? (Guid?)Guid.Parse(guid) : null;
        }

        public static bool IsEmptyOrValidGuid(string guid)
        {
            return string.IsNullOrEmpty(guid) || IsValidGuid(guid);
        }

        private static TResponse GetResponse<TResponse>(bool isSuccess, string message)
        {
            var responseObject = (TResponse)Activator.CreateInstance(typeof(TResponse));

            // Set Status Code
            var statusObject = (StatusResult)Activator.CreateInstance(typeof(StatusResult));

            statusObject.GetType().GetProperty(nameof(StatusResult.IsSuccess)).SetValue(statusObject, isSuccess);
            statusObject.GetType().GetProperty(nameof(StatusResult.Message)).SetValue(statusObject, message);

            responseObject.GetType().GetProperty("Status").SetValue(responseObject, statusObject);

            return responseObject;
        }

        public static TResponse GetResponse<TResponse>(StatusCode statusCode, IStringLocalizer<SharedResource> stringLocalizer)
        {
            string message = statusCode.GetMessage(stringLocalizer);
            return GetResponse<TResponse>(statusCode == StatusCode.Success, message);
        }
        public static TResponse GetFailResponse<TResponse>(string message)
        {
            return GetResponse<TResponse>(false, message);
        }

        public static string ToGuidString(this Guid guid)
        {
            return guid == Guid.Empty ? string.Empty : guid.ToString();
        }

        public static Timestamp GetTimestamp(this DateTime? dateTime)
        {
            if (!dateTime.HasValue)
            {
                return null;
            }

            long seconds = ((DateTimeOffset)dateTime).ToUnixTimeSeconds();
            long milliseconds = ((DateTimeOffset)dateTime).ToUnixTimeMilliseconds();

            var timestamp = new Timestamp
            {
                Seconds = seconds,
                Nanos = (int)(milliseconds%(seconds*1e3)*1e6)
            };

            return timestamp;
        } 
    }
}
