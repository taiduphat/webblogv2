﻿using Microsoft.Extensions.Localization;
using WebBlog.Api.Resources;

namespace WebBlog.Api.Helper
{
    public static class EnumExtension
    {
        public static string GetMessage(this WebBlog.Domain.Enums.StatusCode statusCode, IStringLocalizer<SharedResource> stringLocalizer)
        {
            return stringLocalizer[statusCode.ToString()];
        }
    }
}
