﻿ using AutoMapper;
using Grpc.WebBlogService;
using System;
using System.Linq;
using WebBlog.Api.Helper;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Models;
using static Grpc.WebBlogService.GetFieldsByTemplateResponse.Types;

namespace WebBlog.Api
{
    public class GrpcMappings : Profile
    {
        public GrpcMappings()
        {
            CreateMap<FieldType, FieldTypeDto>();
            CreateMap<FieldTypeDto, FieldType>();

            CreateMap<FieldRequest, FieldDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)))
                .ForMember(dest => dest.DataSource, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.DataSource)));
            CreateMap<InsertOrUpdateTemplateRequest, TemplateDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)));
            
            CreateMap<TemplateDto, TemplateResponse>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()))
                .ForMember(dest => dest.Fields, opt => opt.MapFrom(x => x.Fields));

            CreateMap<FieldDto, FieldResponse>()
                .ForMember(dest => dest.DataSource, opt => opt.MapFrom(x => x.DataSource.ToString() ?? string.Empty))
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()));

            CreateMap<ItemValueRequest, ItemValueDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)));
            CreateMap<InsertOrUpdateItemRequest, ItemDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)))
                .ForMember(dest => dest.ParentItemId, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.ParentItemId)))
                .ForMember(dest => dest.Values, opt => opt.MapFrom(x => x.ItemValues));


            CreateMap<ItemValueDto, ItemValueResponse>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()));
                //.ForMember(dest => dest.FieldName, opt => opt.MapFrom(x => x.Field.Name));
            CreateMap<ItemDto, ItemResponse>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Id.HasValue ? x.Id.ToString() : string.Empty))
                .ForMember(dest => dest.ParentItemId, opt => opt.MapFrom(x => x.ParentItemId.HasValue ? x.ParentItemId.ToString() : string.Empty))
                .ForMember(dest => dest.ItemValues, opt => opt.MapFrom(x => x.Values))
                .ForMember(dest => dest.ChildItems, opt => opt.MapFrom(x => x.ChildItems));

            CreateMap(typeof(PagedList<>), typeof(PagedList));
            //CreateMap<FileStorageDto, FileResponse>()
            //    .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => Google.Protobuf.WellKnownTypes.Timestamp.FromDateTime(x.CreatedOn)))
            //    .ForMember(dest => dest.Content, opt => opt.MapFrom(x => Google.Protobuf.ByteString.Empty))
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Id.HasValue ? x.Id.ToString() : string.Empty));
        }
    }
}
