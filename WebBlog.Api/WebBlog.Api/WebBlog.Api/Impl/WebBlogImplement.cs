﻿using AutoMapper;
using Grpc.Core;
using Grpc.WebBlogService;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBlog.Api.Helper;
using WebBlog.Api.Resources;
using WebBlog.Application.Common.Dtos;
using WebBlog.Application.Common.Models;
using WebBlog.Application.Fields.Commands.DeleteFields;
using WebBlog.Application.Fields.Commands.DeleteTemplate;
using WebBlog.Application.Fields.Queries.GetFieldsByTemplateId;
using WebBlog.Application.FieldTypes.Queries.GetFieldTypes;
using WebBlog.Application.Files.Commands.DeleteFile;
using WebBlog.Application.Files.Commands.UploadFile;
using WebBlog.Application.Files.Queries;
using WebBlog.Application.Items.Commands.CreateItem;
using WebBlog.Application.Items.Commands.DeleteItem;
using WebBlog.Application.Items.Commands.UpdateItem;
using WebBlog.Application.Items.Queries.GetItemById;
using WebBlog.Application.Items.Queries.GetItems;
using WebBlog.Application.Templates.Commands.CreateTemplate;
using WebBlog.Application.Templates.Queries.GetTemplates;
using static Grpc.WebBlogService.GetFieldsByTemplateResponse.Types;

namespace WebBlog.Api.Impl
{
    [Authorize()]
    public class WebBlogImplement : Webblog.WebblogBase
    {
        private readonly IStringLocalizer<SharedResource> _stringLocalizer;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public WebBlogImplement(IMediator mediator,
            IMapper mapper,
            IStringLocalizer<SharedResource> stringLocalizer)
        {
            _mediator = mediator;
            _stringLocalizer = stringLocalizer;
            _mapper = mapper;
        }

        public override async Task<GetFieldsByTemplateResponse> GetFieldsByTemplate(GetFieldsByTemplateRequest request, ServerCallContext context)
        {
            var templateId = Guid.Parse(request.TemplateId);
            var serviceResult = await _mediator.Send(new GetFieldsByTemplateIdQuery { TemplateId = templateId });

            var model = CommonHelper.GetResponse<GetFieldsByTemplateResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result?.Count > 0)
            {
                var result = _mapper.Map<List<FieldResponse>>(serviceResult.Result);
                model.Fields.AddRange(result);
            }

            return model;
        }

        public override async Task<GetFieldTypesResponse> GetFieldTypes(Google.Protobuf.WellKnownTypes.Empty request, ServerCallContext context)
        {
            var serviceResult = await _mediator.Send(new GetFieldTypesQuery());

            var model = CommonHelper.GetResponse<GetFieldTypesResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result?.Count > 0)
            {
                var fieldTypes = _mapper.Map<IList<FieldType>>(serviceResult.Result);
                model.FieldTypes.AddRange(fieldTypes);
            }

            return model;
        }

        public override async Task<GetFilesResponse> GetFiles(GetFilesRequest request, ServerCallContext context)
        {
            var serviceResult = await _mediator.Send(new GetFileInfosQuery
            {
                Page = request.Page,
                PageSize = request.PageSize
            });

            var model = CommonHelper.GetResponse<GetFilesResponse>(serviceResult.StatusCode, _stringLocalizer);

            var pagedList = _mapper.Map<PagedList>(serviceResult.Result);
            model.PagedList = pagedList;

            foreach (var file in serviceResult.Result.Results)
            {
                var content = await _mediator.Send(new GetFileContentQuery(file.Id.Value));
                file.Content = content.Result;

                model.Files.Add(new FileResponse
                {
                    Id = file.Id.ToString(),
                    FileName = file.FileName,
                    Content = Google.Protobuf.ByteString.CopyFrom(file.Content),
                    CreatedOn = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTime(file.CreatedOn)
                });
            }

            return model;
        }

        public override async Task<GetItemByIdResponse> GetItemById(GetItemByIdRequest request, ServerCallContext context)
        {
            var itemId = Guid.Parse(request.ItemId);
            var serviceResult = await _mediator.Send(new GetItemByIdQuery { ItemId = itemId });

            var model = CommonHelper.GetResponse<GetItemByIdResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result != null)
                model.Item = _mapper.Map<ItemResponse>(serviceResult.Result);

            return model;
        }

        public override async Task<GetItemsResponse> GetItems(GetItemsRequest request, ServerCallContext context)
        {
            var query = new GetItemsQuery
            {
                ParentItemId = CommonHelper.ParseGuid(request.ParentItemId),
                IsGetDescendants = request.IsGetDescendants
            };
            var serviceResult = await _mediator.Send(query);

            var model = CommonHelper.GetResponse<GetItemsResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result?.Count > 0)
            {
                var items = _mapper.Map<IList<ItemResponse>>(serviceResult.Result);
                model.Items.AddRange(items);
            }

            return model;
        }

        public override async Task<GetTemplateByIdResponse> GetTemplateById(GetTemplateByIdRequest request, ServerCallContext context)
        {
            var templateId = Guid.Parse(request.TemplateId);
            var serviceResult = await _mediator.Send(new GetTemplateByIdQuery { TemplateId = templateId });

            var model = CommonHelper.GetResponse<GetTemplateByIdResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result != null)
                model.Template = _mapper.Map<TemplateResponse>(serviceResult.Result);

            return model;
        }

        public override async Task<GetTemplatesResponse> GetTemplates(GetTemplatesRequest request, ServerCallContext context)
        {
            var serviceResult = await _mediator.Send(new GetTemplatesQuery());
            var result = _mapper.Map<List<TemplateResponse>>(serviceResult.Result);

            var model = CommonHelper.GetResponse<GetTemplatesResponse>(serviceResult.StatusCode, _stringLocalizer);
            model.Templates.AddRange(result);

            return model;
        }

        public override async Task<InsertOrUpdateItemResponse> InsertOrUpdateItem(InsertOrUpdateItemRequest request, ServerCallContext context)
        {
            var entityDto = _mapper.Map<ItemDto>(request);
            ServiceResult<Guid> serviceResult;

            if (entityDto.Id.HasValue)
                serviceResult = await _mediator.Send(new UpdateItemCommand { Item = entityDto });
            else
                serviceResult = await _mediator.Send(new CreateItemCommand { Item = entityDto });

            var model = CommonHelper.GetResponse<InsertOrUpdateItemResponse>(serviceResult.StatusCode, _stringLocalizer);
            model.ItemId = serviceResult.Result.ToGuidString();

            return model;
        }

        public override async Task<InsertOrUpdateTemplateResponse> InsertOrUpdateTemplate(InsertOrUpdateTemplateRequest request, ServerCallContext context)
        {
            var templateDto = _mapper.Map<TemplateDto>(request);
            ServiceResult<Guid> serviceResult;

            if (!templateDto.Id.HasValue)
                serviceResult = await _mediator.Send(new CreateTemplateCommand { Template = templateDto });
            else
                serviceResult = await _mediator.Send(new UpdateTemplateCommand { Template = templateDto });

            var model = CommonHelper.GetResponse<InsertOrUpdateTemplateResponse>(serviceResult.StatusCode, _stringLocalizer);
            model.TemplateId = serviceResult.Result.ToGuidString();

            return model;
        }

        public override async Task<BaseResponse> RemoveFields(RemoveFieldsRequest request, ServerCallContext context)
        {
            var templateId = Guid.Parse(request.TemplateId);

            List<Guid> removeFieldIds = new List<Guid>();
            foreach (var item in request.FieldIds)
                removeFieldIds.Add(Guid.Parse(item));

            var serviceResult = await _mediator.Send(new DeleteFieldsByTemplateIdCommand
            {
                TemplateId = templateId,
                RemoveFieldIds = removeFieldIds.ToArray()
            });

            var model = CommonHelper.GetResponse<BaseResponse>(serviceResult.StatusCode, _stringLocalizer);
            return model;
        }

        public override async Task<BaseResponse> RemoveFile(RemoveFileRequest request, ServerCallContext context)
        {
            var fileId = Guid.Parse(request.FileId);
            //var serviceResult = await _fileStorageService.RemoveFile(fileId);
            var serviceResult = await _mediator.Send(new DeleteFileCommand { FileId = fileId });

            var model = CommonHelper.GetResponse<BaseResponse>(serviceResult.StatusCode, _stringLocalizer);
            return model;
        }

        public override async Task<BaseResponse> RemoveItem(RemoveItemRequest request, ServerCallContext context)
        {
            var itemId = CommonHelper.ParseGuid(request.ItemId);
            if (!itemId.HasValue)
            {
                string message = string.Format(_stringLocalizer["InvalidGuidError"], nameof(request.ItemId));
                return CommonHelper.GetFailResponse<BaseResponse>(message);
            }

            //var serviceResult = await _itemService.Remove(itemId.Value);
            var serviceResult = await _mediator.Send(new DeleteItemCommand { ItemId = itemId.Value });

            var model = CommonHelper.GetResponse<BaseResponse>(serviceResult.StatusCode, _stringLocalizer);
            return model;
        }

        public override async Task<BaseResponse> RemoveTemplate(RemoveTemplateRequest request, ServerCallContext context)
        {
            var templateId = CommonHelper.ParseGuid(request.TemplateId);
            if (!templateId.HasValue)
            {
                string message = string.Format(_stringLocalizer["InvalidGuidError"], nameof(request.TemplateId));
                return CommonHelper.GetFailResponse<BaseResponse>(message);
            }

            var serviceResult = await _mediator.Send(new DeleteTemplateCommand { TemplateId = templateId.Value });

            var model = CommonHelper.GetResponse<BaseResponse>(serviceResult.StatusCode, _stringLocalizer);
            return model;
        }

        public override async Task<BaseResponse> UploadFile(UploadFileRequest request, ServerCallContext context)
        {
            var serviceResult = await _mediator.Send(new UploadFileCommand
            {
                FileName = request.FileName,
                Content = request.Content.ToArray(),
                ShouldOverwrite = request.IsOverwrite

            });

            var model = CommonHelper.GetResponse<BaseResponse>(serviceResult.StatusCode, _stringLocalizer);
            return model;
        }
    }
}
