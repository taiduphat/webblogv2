﻿//using WebBlog.Repository.Context;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Design;
//using Microsoft.Extensions.Configuration;
//using System;
//using System.IO;

//namespace WebBlog.Api.Factories
//{
//    public class BlogContextFactory : IDesignTimeDbContextFactory<BlogContext>
//    {
//        public BlogContext CreateDbContext(string[] args)
//        {
//            // Get environment
//            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

//            // Build config
//            IConfiguration config = new ConfigurationBuilder()
//                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
//                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
//                .AddJsonFile($"appsettings.{environment}.json", optional: true)
//                .AddEnvironmentVariables()
//                .Build();

//            // Get connection string
//            var optionsBuilder = new DbContextOptionsBuilder<BlogContext>();
//            optionsBuilder.UseSqlite(config.GetConnectionString("DefaultConnection"));

//            return new BlogContext(optionsBuilder.Options);
//        }
//    }
//}
