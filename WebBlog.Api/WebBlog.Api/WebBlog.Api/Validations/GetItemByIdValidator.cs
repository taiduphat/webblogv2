﻿using FluentValidation;
using Grpc.WebBlogService;
using Microsoft.Extensions.Localization;
using WebBlog.Api.Helper;
using WebBlog.Api.Resources;

namespace WebBlog.Api.Validations
{
    public class GetItemByIdValidator : AbstractValidator<GetItemByIdRequest>
    {
        public GetItemByIdValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.ItemId).Must(x => CommonHelper.IsValidGuid(x))
                .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.ItemId)));
        }
    }
}
