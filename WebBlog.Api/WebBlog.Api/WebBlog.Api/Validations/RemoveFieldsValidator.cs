﻿using FluentValidation;
using Grpc.WebBlogService;
using Microsoft.Extensions.Localization;
using System.Linq;
using WebBlog.Api.Helper;
using WebBlog.Api.Resources;

namespace WebBlog.Api.Validations
{
    public class RemoveFieldsValidator : AbstractValidator<RemoveFieldsRequest>
    {
        public RemoveFieldsValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.TemplateId).Must(x => CommonHelper.IsValidGuid(x))
                .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.TemplateId)));

            RuleFor(m => m.FieldIds)
                .NotEmpty()
                .WithMessage(x => string.Format(stringLocalizer["RequiredError"], nameof(x.FieldIds)));

            When(m => m.FieldIds.Any(), () =>
            {
                RuleFor(x => x.FieldIds).ForEach(rules =>
                {
                    rules.Must(field => CommonHelper.IsValidGuid(field))
                        .WithMessage((list, FieldId) => string.Format(stringLocalizer["InvalidGuidError"], nameof(FieldId)));
                });

                RuleFor(x => x.FieldIds).Must(fields => !fields.GroupBy(x => x).Any(g => g.Count() > 1))
                    .WithMessage(x => string.Format(stringLocalizer["DuplicatedError"], nameof(x.FieldIds)));
            });
        }
    }
}
