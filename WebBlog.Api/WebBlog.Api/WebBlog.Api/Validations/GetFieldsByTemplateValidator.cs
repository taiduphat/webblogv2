﻿using FluentValidation;
using Grpc.WebBlogService;
using Microsoft.Extensions.Localization;
using WebBlog.Api.Helper;
using WebBlog.Api.Resources;

namespace WebBlog.Api.Validations
{
    public class GetFieldsByTemplateValidator : AbstractValidator<GetFieldsByTemplateRequest>
    {
        public GetFieldsByTemplateValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.TemplateId).Must(x => CommonHelper.IsValidGuid(x))
                .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.TemplateId)));
        }
    }
}
