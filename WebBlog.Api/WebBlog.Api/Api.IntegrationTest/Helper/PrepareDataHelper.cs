﻿using Fare;
using Grpc.WebBlogService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebBlog.Api.Helper;
using WebBlog.Domain;
using Xunit;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Helper
{
    public class PrepareDataHelper
    {
        private static readonly Xeger _xeger = new(CommonHelper.SlugRegex, new Random());

        public static InsertOrUpdateTemplateRequest GenerateTemplateRequest()
        {
            var templateRequest = new InsertOrUpdateTemplateRequest
            {
                Name = $"Name_{Guid.NewGuid()}",
                Slug = _xeger.Generate(),
                Fields = 
                {
                    new FieldRequest
                    {
                        Name = $"FieldName_{Guid.NewGuid()}",
                        FieldTypeId = Constants.PlainTextId.ToString(),
                    },
                    new FieldRequest
                    {
                        Name = $"FieldName_{Guid.NewGuid()}",
                        FieldTypeId = Constants.RichTextId.ToString(),
                    }
                }
            };

            return templateRequest;
        }

        public static InsertOrUpdateItemRequest GenerateItemRequest(string itemId, string templateId, List<ItemValueRequest> itemValues)
        {
            var itemRequest = new InsertOrUpdateItemRequest
            {
                Id = itemId,
                Name = $"Name_{Guid.NewGuid()}",
                Slug = _xeger.Generate(),
                TemplateId = templateId,
                ItemValues = { itemValues }
            };

            return itemRequest;
        }

        public static async Task<TemplateResponse> AddingTemplate(WebblogClient client)
        {
            var insertTemplateRequest = GenerateTemplateRequest();

            // Act
            var response = await client.InsertOrUpdateTemplateAsync(insertTemplateRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.TemplateId));

            var getTemplateResponse = await client.GetTemplateByIdAsync(new GetTemplateByIdRequest { TemplateId = response.TemplateId });

            return getTemplateResponse.Template;
        }

        public static UploadFileRequest GenerateUploadFileRequest(string fileName)
        {
            string content = $"Generated content: {Guid.NewGuid().ToString()}";
            return new UploadFileRequest
            {
                FileName = fileName,
                Content = Google.Protobuf.ByteString.CopyFromUtf8(content)
            };
        }
    }
}
