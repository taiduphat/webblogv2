﻿using Api.IntegrationTest.Fixture;
using Api.IntegrationTest.Helper;
using Grpc.WebBlogService;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Scenarios
{
    [Collection(Fixture.TestConstants.UnitTest_Collection_Name)]
    public class GetFieldsByTemplateTest : IClassFixture<TestFixture>
    {
        private readonly WebblogClient _client;

        public GetFieldsByTemplateTest(TestFixture testFixture)
        {
            _client = testFixture.Client;
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        public async Task GetFields_WithInvalidTemplateId_Fail(string templateId)
        {
            var request = new GetFieldsByTemplateRequest()
            {
                TemplateId = templateId
            };

            // Act
            var response = await _client.GetFieldsByTemplateAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && !response.Fields.Any());
        }

        [Fact]
        public async Task GetFields_WithValidTemplateId_Success()
        {
            var insertTemplateRequest = PrepareDataHelper.GenerateTemplateRequest();
            var insertResp = await _client.InsertOrUpdateTemplateAsync(insertTemplateRequest);

            Assert.True(insertResp != null && insertResp.Status.IsSuccess);

            var getFieldsRequest = new GetFieldsByTemplateRequest
            {
                TemplateId = insertResp.TemplateId
            };
            // Act
            var response = await _client.GetFieldsByTemplateAsync(getFieldsRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && response.Fields.Any());
        }
    }
}
