﻿using Api.IntegrationTest.Fixture;
using Api.IntegrationTest.Helper;
using Google.Protobuf;
using Grpc.WebBlogService;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Scenarios
{
    [Collection(Fixture.TestConstants.UnitTest_Collection_Name)]
    public class ManageFileTest : IClassFixture<TestFixture>
    {
        private readonly WebblogClient _client;

        public ManageFileTest(TestFixture testFixture)
        {
            _client = testFixture.Client;
        }

        [Fact]
        public async Task UploadFile_ShouldRunSuccess()
        {
            string fileName = "NewFile.txt";
            var request = PrepareDataHelper.GenerateUploadFileRequest(fileName);
            // Act
            var result = await _client.UploadFileAsync(request);

            // Assert
            Assert.True(result != null && result.Status.IsSuccess);
        }

        [Fact]
        public async Task UploadFile_WhichOverwriteFile_ShouldRunSuccess()
        {
            string fileName = $"NewFile{Guid.NewGuid()}.txt";
            var request = PrepareDataHelper.GenerateUploadFileRequest(fileName);
            var result = await _client.UploadFileAsync(request);
            Assert.True(result != null && result.Status.IsSuccess);
            
            // Act
            var request2 = PrepareDataHelper.GenerateUploadFileRequest(fileName);
            request2.IsOverwrite = true;
            
            // Assert
            var result2 = await _client.UploadFileAsync(request2);
            Assert.True(result2 != null && result2.Status.IsSuccess);
        }

        [Fact]
        public async Task GetFiles_ShouldRunSuccess()
        {
            string fileName = $"NewFile{Guid.NewGuid()}.txt";
            var request = PrepareDataHelper.GenerateUploadFileRequest(fileName);
            // Act
            var uploadResult = await _client.UploadFileAsync(request);
            Assert.True(uploadResult != null && uploadResult.Status.IsSuccess);

            var getFilesRequest = new GetFilesRequest
            {
                Page = 0,
                PageSize = 10
            };
            // Act
            var result = await _client.GetFilesAsync(getFilesRequest);

            // Assert
            Assert.True(result != null && result.Status.IsSuccess && result.Files.Any());
        }
    }
}
