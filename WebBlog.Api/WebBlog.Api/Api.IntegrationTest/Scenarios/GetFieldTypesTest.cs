﻿using Api.IntegrationTest.Fixture;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Scenarios
{
    [Collection(Fixture.TestConstants.UnitTest_Collection_Name)]
    public class GetFieldTypesTest : IClassFixture<TestFixture>
    {
        private readonly WebblogClient _client;

        public GetFieldTypesTest(TestFixture testFixture)
        {
            _client = testFixture.Client;
        }

        [Fact]
        public async Task GetFieldTypes_ShouldRunSuccess()
        {
            // Act
            var result = await _client.GetFieldTypesAsync(new Google.Protobuf.WellKnownTypes.Empty());

            // Assert
            Assert.True(result != null && result.Status.IsSuccess && result.FieldTypes.Any());
        }
    }
}
