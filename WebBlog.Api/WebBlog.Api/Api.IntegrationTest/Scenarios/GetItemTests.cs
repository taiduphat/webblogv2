﻿using Api.IntegrationTest.Fixture;
using Api.IntegrationTest.Helper;
using Grpc.WebBlogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Scenarios
{
    [Collection(Fixture.TestConstants.UnitTest_Collection_Name)]
    public class GetItemTests : IClassFixture<TestFixture>
    {
        private readonly WebblogClient _client;

        public GetItemTests(TestFixture testFixture)
        {
            _client = testFixture.Client;
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        public async Task GetItemById_WithInvalidId_Fail(string itemId)
        {
            // Arrange 
            var request = new GetItemByIdRequest
            {
                ItemId = itemId
            };

            // Act
            var result = await _client.GetItemByIdAsync(request);

            // Assert
            Assert.NotNull(result);
            Assert.True(!result.Status.IsSuccess && result.Item == null);
        }

        [Fact]
        public async Task GetItemById_WithValidData_Success()
        {
            var prepareTemplate = await PrepareDataHelper.AddingTemplate(_client);
            string templateId = prepareTemplate.Id.ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                Id = Guid.NewGuid().ToString(),
                FieldId = prepareTemplate.Fields.First().Id,
                Value = $"Value_{Guid.NewGuid()}"
            });
            var prepareItemResponse = await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            var getItemRequest = new GetItemByIdRequest
            {
                ItemId = prepareItemResponse.ItemId
            };
            // Act
            var result = await _client.GetItemByIdAsync(getItemRequest);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status.IsSuccess);
            Assert.True(result.Item.Id == getItemRequest.ItemId);
        }

        [Fact]
        public async Task GetItems_WithValidData_Success()
        {
            var prepareTemplate = await PrepareDataHelper.AddingTemplate(_client);
            string templateId = prepareTemplate.Id.ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = prepareTemplate.Fields.First().Id,
                Value = $"Value_{Guid.NewGuid()}"
            });
            await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            var getItemRequest = new GetItemsRequest();
            // Act
            var result = await _client.GetItemsAsync(getItemRequest);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status.IsSuccess);
            Assert.True(result.Items.Any());
        }
    }
}
