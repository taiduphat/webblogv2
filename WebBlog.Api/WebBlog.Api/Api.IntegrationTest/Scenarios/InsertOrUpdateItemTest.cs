﻿using Api.IntegrationTest.Fixture;
using Api.IntegrationTest.Helper;
using Grpc.WebBlogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Scenarios
{
    [Collection(Fixture.TestConstants.UnitTest_Collection_Name)]
    public class InsertOrUpdateItemTest : IClassFixture<TestFixture>
    {
        private readonly WebblogClient _client;
        public InsertOrUpdateItemTest(TestFixture testFixture)
        {
            _client = testFixture.Client;
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        [InlineData("a2bdd9ca-ea15")]
        public async Task InsertOrUpdateItem_WhichInvalidTemplateId_Fail(string templateId)
        {
            var request = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());

            // Act
            var response = await _client.InsertOrUpdateItemAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.ItemId));
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        [InlineData("a2bdd9ca-ea15")]
        public async Task InsertOrUpdateItem_WhichInvalidFieldId_Fail(string fieldId)
        {
            var itemValues = new List<ItemValueRequest>
            {
                new ItemValueRequest
                {
                    FieldId = fieldId,
                    Value = $"Value_{Guid.NewGuid()}"
                }
            };
            var request = PrepareDataHelper.GenerateItemRequest(string.Empty, Guid.NewGuid().ToString(), itemValues);

            // Act
            var response = await _client.InsertOrUpdateItemAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.ItemId));
        }

        [Fact]
        public async Task InsertOrUpdateItem_WhichInvalidItemId_Fail()
        {
            string invalidId = Guid.NewGuid().ToString().Substring(0, 10);
            var request = PrepareDataHelper.GenerateItemRequest(invalidId, Guid.NewGuid().ToString(), new List<ItemValueRequest>());

            // Act
            var response = await _client.InsertOrUpdateItemAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.ItemId));
        }

        [Fact]
        public async Task InsertItem_WhichValidData_Success()
        {
            // Prepare template
            var prepareTemplate = await PrepareDataHelper.AddingTemplate(_client);
            string templateId = prepareTemplate.Id.ToString();

            var itemValues = new List<ItemValueRequest>();
            foreach (var fieldId in prepareTemplate.Fields.Select(x => x.Id))
            {
                itemValues.Add(new ItemValueRequest
                {
                    FieldId = fieldId,
                    Value = $"Value_{Guid.NewGuid()}"
                });
            }
            
            var insertItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, itemValues);

            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.ItemId));
        }

        [Fact]
        public async Task UpdateItem_BothAddsAndUpdatesNewField_Success()
        {
            var prepareTemplate = await PrepareDataHelper.AddingTemplate(_client);
            string templateId = prepareTemplate.Id.ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = prepareTemplate.Fields.First().Id,
                Value = $"Value_{Guid.NewGuid()}"
            });
            var prepareItemResponse = await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            var getItemResponse = await _client.GetItemByIdAsync(new GetItemByIdRequest { ItemId = prepareItemResponse.ItemId });
            var item = getItemResponse.Item;

            var insertItemRequest = new InsertOrUpdateItemRequest
            {
                Id = item.Id,
                Name = item.Name,
                Slug = item.Slug,
                TemplateId = item.TemplateId,
                ParentItemId = item.ParentItemId,
                ItemValues = {
                    item.ItemValues.Select(x => new ItemValueRequest
                    {
                        Id = x.Id,
                        FieldId = x.FieldId,
                        Value = x.Value
                    }) 
                }
            };

            // Update new value of old field
            insertItemRequest.ItemValues[0].Value = Guid.Empty.ToString();
            // Add new field
            insertItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = prepareTemplate.Fields.Last().Id,
                Value = $"Value_{Guid.NewGuid()}"
            });
            
            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.ItemId));

            var getItemRequest = new GetItemByIdRequest { ItemId = response.ItemId };
            var itemValues = (await _client.GetItemByIdAsync(getItemRequest)).Item.ItemValues;

            var result = from x in insertItemRequest.ItemValues
                      join y in itemValues on new { x.FieldId, x.Value } equals new { y.FieldId, y.Value }
                      select new { y.Id, y.FieldId, y.Value };
            Assert.True(result.Count() == itemValues.Count);
        }

        [Fact]
        public async Task InsertItem_WhichValidParentItem_Success()
        {
            var prepareTemplate = await PrepareDataHelper.AddingTemplate(_client);
            string templateId = prepareTemplate.Id.ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = prepareTemplate.Fields.First().Id,
                Value = $"Value_{Guid.NewGuid()}"
            });
            var insertItemRequest = prepareItemRequest.Clone();

            var prepareItemResponse = await _client.InsertOrUpdateItemAsync(prepareItemRequest);
            
            insertItemRequest.ParentItemId = prepareItemResponse.ItemId;
            // Update new value of old field
            insertItemRequest.ItemValues[0].Value = Guid.Empty.ToString();
            // Add new field
            insertItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = prepareTemplate.Fields.Last().Id,
                Value = $"Value_{Guid.NewGuid()}"
            });

            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.ItemId));

            var getItemRequest = new GetItemByIdRequest { ItemId = response.ItemId };
            var item = (await _client.GetItemByIdAsync(getItemRequest)).Item;
            
            Assert.True(item.ParentItemId == prepareItemResponse.ItemId);
        }

        [Fact]
        public async Task InsertItem_WhichInvalidParentItem_Fail()
        {
            var prepareTemplate = await PrepareDataHelper.AddingTemplate(_client);
            string templateId = prepareTemplate.Id.ToString();

            var itemValues = new List<ItemValueRequest>();
            foreach (var fieldId in prepareTemplate.Fields.Select(x => x.Id))
            {
                itemValues.Add(new ItemValueRequest
                {
                    FieldId = fieldId,
                    Value = $"Value_{Guid.NewGuid()}"
                });
            }

            var insertItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, itemValues);
            insertItemRequest.ParentItemId = Guid.NewGuid().ToString();
            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess);
        }
    }
}
