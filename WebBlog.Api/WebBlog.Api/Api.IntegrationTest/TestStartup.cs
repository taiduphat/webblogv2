﻿using Api.IntegrationTest.Fixture;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebBlog.Api;

namespace Api.IntegrationTest
{
    internal class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthentication(TestConstants.AuthenticationScheme)
                .AddScheme<AuthenticationSchemeOptions, TestAuthenticationHandler>(TestConstants.AuthenticationScheme, null);

            services.AddAuthorization();
        }
    }
}
