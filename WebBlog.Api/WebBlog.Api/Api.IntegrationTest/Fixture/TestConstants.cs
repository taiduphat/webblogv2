﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.IntegrationTest.Fixture
{
    public static class TestConstants
    {
        public const string UnitTest_Collection_Name = "UnitTest Collection";
        public const string AuthenticationScheme = "Test Schema";
    }
}
