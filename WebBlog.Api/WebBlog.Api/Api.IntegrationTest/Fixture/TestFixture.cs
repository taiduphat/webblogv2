using Api.IntegrationTest.Fixture;
using static Grpc.WebBlogService.Webblog;

namespace Api.IntegrationTest.Fixture
{
    public class TestFixture
    {
        public WebblogClient Client;

        public TestFixture()
        {
            Client = new WebblogClient(CollectionFixture.Channel);
        }
    }
}
