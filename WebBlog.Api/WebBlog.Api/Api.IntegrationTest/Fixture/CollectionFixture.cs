﻿using Grpc.Net.Client;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using WebBlog.Infrastructure.Persistence;

namespace Api.IntegrationTest.Fixture
{
    public class CollectionFixture : IDisposable
    {
        private readonly TestServer _testServer;
        public static GrpcChannel Channel;

        public CollectionFixture()
        {
            var webBuilder = WebHost.CreateDefaultBuilder()
                .UseWebRoot(Directory.GetCurrentDirectory())
                .UseStartup<TestStartup>();

            _testServer = new TestServer(webBuilder);

            // Create a scope to obtain a reference to the database
            using var scope = _testServer.Services.CreateScope();
            var scopedServices = scope.ServiceProvider;
            var blogContext = scopedServices.GetRequiredService<ApplicationDbContext>();
            // Ensure the database is created.
            blogContext.Database.EnsureCreated();
            
            var client = _testServer.CreateClient();
            Channel = GrpcChannel.ForAddress(client.BaseAddress, new GrpcChannelOptions { HttpClient = client });
        }

        public void Dispose()
        {
            _testServer.Dispose();
            if (Channel != null)
            {
                Channel.Dispose();
            }
            GC.SuppressFinalize(this);


            Console.WriteLine("Dispose was called by CollectionFixture");
        }

    }
}
