﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Api.IntegrationTest.Fixture
{
    [CollectionDefinition(TestConstants.UnitTest_Collection_Name)]
    public class CollectionFixtureDefinition : ICollectionFixture<CollectionFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
