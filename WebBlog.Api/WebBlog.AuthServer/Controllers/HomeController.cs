﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebBlog.AuthServer.Models;

namespace WebBlog.AuthServer.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Error()
        {
            return View(new ErrorViewModel());
        }

        [Authorize]
        public IActionResult Index()
        {
            return View(User.Identity);
        }
    }
}