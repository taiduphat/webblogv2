﻿namespace WebBlog.AuthServer
{
    public static class AuthServerConstants
    {
        public static class Scope
        {
            public const string GrpcService = "grpc.service";
        }

        public static class Role
        {
            public const string User = "user";
            public const string Administrator = "administrator";
        }

        public static class ClaimType
        {
            public const string FullName = "fullname";
            public const string CreatedAt = "createdat";
            public const string Permission = "permission";
        }
    }
}
