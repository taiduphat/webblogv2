﻿using System.Security.Claims;

namespace WebBlog.AuthServer.Helpers
{
    public class ClaimsHelper
    {
        public static Claim CreatePermissionClaim(string permissionValue)
        {
            return new Claim(AuthServerConstants.ClaimType.Permission, permissionValue.Trim());
        }
    }
}
