﻿using System.Collections.Generic;

namespace WebBlog.AuthServer.Models
{
    public class PermissionViewModel
    {
        public string RoleId { get; set; }
        public IList<string> RoleClaims { get; set; }
    }
}
