namespace WebBlog.AuthServer.Models
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}