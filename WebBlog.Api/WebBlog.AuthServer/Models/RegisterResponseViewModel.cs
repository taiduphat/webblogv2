﻿using Microsoft.AspNetCore.Identity;

namespace WebBlog.AuthServer.Models
{
    public class RegisterResponseViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
    }
}