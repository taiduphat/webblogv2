﻿using IdentityModel;
using IdentityServer4.Models;
using System.Collections.Generic;
using static WebBlog.AuthServer.AuthServerConstants;

namespace WebBlog.AuthServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            var profileResource = new IdentityResources.Profile();
            profileResource.UserClaims.Add(ClaimType.FullName);

            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                profileResource,
                new IdentityResource("roles", "User roles", new[] { JwtClaimTypes.Role, ClaimType.Permission })
            };
        }
    }
}
